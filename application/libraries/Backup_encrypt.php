<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BACKUP_Encrypt extends CI_Encrypt
{

	public function custom_encode($string, $url_safe = TRUE) {		$key = config_item('encryption_key');
	    $ret = parent::encode($string, $key);
	    if ($url_safe) {
	        $ret = strtr($ret, array('+' => '.', '=' => '-', '/' => '~'));
	    }

	    return $ret;
	}

	public function custom_decode($string) {
		$key = config_item('encryption_key');
	    $string = strtr($string, array('.' => '+', '-' => '=', '~' => '/'));

	    return parent::decode($string, $key);
	}

}

?>