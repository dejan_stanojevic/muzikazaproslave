<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class zvezdeGranda extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(['ion_auth']);
		$this->load->model('Profile_model');
		$this->load->model('zvezdeGranda_model');
		//$this->load->helper(['url', 'language']);

	}

	public function index()
	{
		$this->lang->load('auth');
		if ($this->ion_auth->logged_in()){

			 $user_info = $this->ion_auth->user()->row();

			 $this->data['user_info'] = array(
				 'user_id' => $user_info->id,
				 'first_name' => $user_info->first_name,
				 'last_name' => $user_info->last_name,
				 'email' => $user_info->email,
				 'username' => $user_info->username,
				 'last_login' => $user_info->last_login,
				 'profile_url' => $user_info->profile_url,
				 'picture_url' => $user_info->picture_url,
			 );
		 }

		 	$data['parties_list'] = $this->Profile_model->get_parties_list();
		 	$data['genres_list'] = $this->Profile_model->get_genres_list();
			$data['bands_type_list'] = $this->Profile_model->get_bands_type_list();
			$data['instruments_list'] = $this->Profile_model->get_instruments_list();
			$data['zvezde_granda'] = $this->zvezdeGranda_model->get_zvezde_granda();


			$this->data['title'] =  "Pronađi muziku | Bendovi za svadbe";
      $this->_render_page('template/header', $this->data);
			$this->_render_page('template/navbar');

			$this->_render_page('zvezdeGranda', $data);
			$this->_render_page('template/footer');

	}
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
