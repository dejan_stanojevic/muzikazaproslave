<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(['ion_auth']);
		$this->load->model('Profile_model');
		//$this->load->helper(['url', 'language']);

	}

	public function index()
	{
		$this->lang->load('auth');
		if ($this->ion_auth->logged_in()){

			 $user_info = $this->ion_auth->user()->row();

			 $this->data['user_info'] = array(
				 'user_id' => $user_info->id,
				 'first_name' => $user_info->first_name,
				 'last_name' => $user_info->last_name,
				 'email' => $user_info->email,
				 'username' => $user_info->username,
				 'last_login' => $user_info->last_login,
				 'profile_url' => $user_info->profile_url,
				 'picture_url' => $user_info->picture_url,
			 );
		 }
		 else {
			 redirect('auth/login', 'refresh');
		 }
		 	$profile_type = $this->Profile_model->get_profile_type($user_info->id);
			$band_id = $this->Profile_model->get_band_id($user_info->id);
			$profile_list = $this->Profile_model->get_profile_list();
			$data['profile_list'] = $profile_list;
		 	$data['profile_type'] = $profile_type;
			$data['parties_list'] = $this->Profile_model->get_parties_list();
			$data['genres_list'] = $this->Profile_model->get_genres_list();
			$data['bands_type_list'] = $this->Profile_model->get_bands_type_list();
			$data['instruments_list'] = $this->Profile_model->get_instruments_list();
			$data['band_details'] = $this->Profile_model->get_band_details($user_info->id);
			$data['band_have_parties'] = $this->Profile_model->get_band_have_parties($user_info->id);
			$data['band_have_genres'] = $this->Profile_model->get_band_have_genres($user_info->id);
			$data['band_have_types'] = $this->Profile_model->get_band_have_types($user_info->id);
			$data['band_have_instruments'] = $this->Profile_model->get_band_have_instruments($user_info->id);
			$data['band_have_photos'] = $this->Profile_model->get_band_have_photos($user_info->id);
			$data['band_have_youtubes'] = $this->Profile_model->get_band_have_youtubes($user_info->id);
			if (!isset($profile_type)) {
			$this->data['title'] =  "Profil";
      $this->_render_page('template/profile/header', $this->data);
			$this->_render_page('template/profile/navbar');

			$this->_render_page('template/profile/profile_type', $data);
			$this->_render_page('template/footer');
		}
		else {
			$profile_url = $this->Profile_model->get_profile_url($user_info->id)->profile_url;
			$this->data['title'] = "Ažuriranje profila";
      $this->_render_page('template/profile/header', $this->data);
			$this->_render_page('template/navbar');

			$this->_render_page('template/profile/profile_' . $profile_url, $data);
			$this->_render_page('template/profile/footer');
		}
	}


	public function crate_user_profile_type()
	{
		$this->lang->load('auth');
		if ($this->ion_auth->logged_in()){
			 $user_info = $this->ion_auth->user()->row();
			 $userID = $user_info->id;
			 $profile_type = $this->input->post('profile_type_name');
			 $data = array(
				 'user_id' => $userID,
				 'profile_id' => $this->input->post('profile_type_id')
			 );
			 $this->Profile_model->crate_user_profile_type($data);
			 $response['message'] = "Nastavite sa ažuriranjem Vašeg ".$profile_type. " profila";
			 header("content-type:application/json");
			 echo json_encode($response);
			 exit;
		 }
		 else {
			 redirect('auth/login', 'refresh');
		 }
		 $this->Profile_model->crate_user_profile_type($data);
	}



	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
