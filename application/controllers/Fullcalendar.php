<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Fullcalendar extends CI_Controller {

 public function __construct()
 {
  parent::__construct();
  $this->load->model('Fullcalendar_model');
  $this->load->model('Profile_model');
 }

 function index()
 {
  $this->load->view('fullcalendar');
 }

 function load()
 {

   $this->lang->load('auth');
   if ($this->ion_auth->logged_in()){
      $user_info = $this->ion_auth->user()->row();
    }
    else {
      redirect('auth/login', 'refresh');
    }

  $profile_type = $this->Profile_model->get_profile_type($user_info->id);
  $band_id = $this->Profile_model->get_band_id($user_info->id);
  $event_data = $this->Fullcalendar_model->fetch_all_event($band_id->id);
  foreach($event_data->result_array() as $row)
  {
   $data[] = array(
    'id' => $row['id'],
    'title' => $row['title'],
    'start' => $row['start_event'],
    'end' => $row['end_event']
   );
  }
  if(isset($data)) {
  echo json_encode($data);
  }
 }

 function insert()
 {
  if($this->input->post('title'))
  {
   $data = array(
		'band_id'   => $this->input->post('band_id'),
    'title'  => $this->input->post('title'),
    'start_event'=> $this->input->post('start'),
    'end_event' => $this->input->post('end')
   );
   $this->Fullcalendar_model->insert_event($data);
  }
 }

 function update()
 {
  if($this->input->post('id'))
  {
   $data = array(
    'title'   => $this->input->post('title'),
    'start_event' => $this->input->post('start'),
    'end_event'  => $this->input->post('end')
   );

   $this->Fullcalendar_model->update_event($data, $this->input->post('id'));
  }
 }

 function delete()
 {
  if($this->input->post('id'))
  {
   $this->Fullcalendar_model->delete_event($this->input->post('id'));
  }
 }

}

?>
