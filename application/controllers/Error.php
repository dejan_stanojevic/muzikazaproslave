<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
        $this->output->set_status_header('404');

        $this->data['site_logo'] = $this->config->item('site_logo');
        $this->data['title'] = 'Not found';
        $this->data['heading'] = '404 Not found';
        $this->data['message'] = lang('error_general');

			$this->load->view('template/header', $this->data);
			$this->load->view('template/navbar_error');
			$this->load->view('error_page/error_404', $this->data);
			$this->load->view('template/footer');
	}
}

/* End of file error.php */
/* Location: ./application/controllers/error.php */