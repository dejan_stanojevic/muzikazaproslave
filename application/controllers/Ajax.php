<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ajax extends CI_Controller {
    function __Construct() {
      parent::__construct();
      $this->load->helper('form');
      $this->load->database();
      $this->load->library(['ion_auth']);
      $this->load->model('Ajax_model');
    }
public function index() {
  {
    $this->lang->load('auth');
    if ($this->ion_auth->logged_in()){

       $user_info = $this->ion_auth->user()->row();

       $this->data['user_info'] = array(
         'user_id' => $user_info->id,
         'first_name' => $user_info->first_name,
         'last_name' => $user_info->last_name,
         'email' => $user_info->email,
         'username' => $user_info->username,
         'last_login' => $user_info->last_login,
         'profile_url' => $user_info->profile_url,
         'picture_url' => $user_info->picture_url,
       );
     }
     else {
       redirect('auth/login', 'refresh');
     }
   }
}

public function uploadheader() {

    $user_info = $this->ion_auth->user()->row();
    $upload_data = array(
        'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/files/header",
        'upload_url' => base_url() . "/files",
        'allowed_types' => "gif|jpg|png|jpeg",
        'overwrite' => TRUE,
        'max_size' => "8048KB",
        'width' => 400,
        'height' => 1,
        'quality' => "100%",
        'file_name' => microtime(true) . rand(1, 10000)  . '.jpg'
    );
    $file_name = null;
    $this->load->library('upload', $upload_data);
    if ($this->upload->do_upload('userfile')) {
        $uploaded_file = $this->upload->data();
        $file_name = $uploaded_file['file_name'];
        $image_config["image_library"] = "gd2";
        $image_config["source_image"] = $upload_data["upload_path"] . "/" . $file_name;
        $image_config['create_thumb'] = FALSE;
        $image_config['maintain_ratio'] = TRUE;
        $image_config['new_image'] = $upload_data["upload_path"] . "/" . $file_name;
        $image_config['quality'] = "100%";
        $image_config['width'] = 1920;
        $image_config['height'] = 1;
        $dim = (intval($upload_data["width"]) / intval($upload_data["height"])) - ($image_config['width'] / $image_config['height']);
        $image_config['master_dim'] = ($dim > 0) ? "height" : "width";
        $this->load->library('image_lib');
        $this->image_lib->initialize($image_config);
        if (!$this->image_lib->resize()) { //Resize image
        }
    }
    if ($file_name == "") {
        //$file_name = 'listing-default.png';
        $data = array(
          'user_id' => $user_info->id,
          'band_cover_image' => $file_name
        );
        $this->Ajax_model->create_header_background($data);
        // $this->output
        //         ->set_content_type('application/json')
        //         ->set_output(json_encode(array('result' => 0)));
        echo json_encode($data);
    } else {

        $data = array(
            'user_id' => $user_info->id,
            'band_cover_image' => $file_name
        );
        $this->Ajax_model->create_header_background($data);
        // $this->output
        //         ->set_content_type('application/json')
        //         ->set_output(json_encode(array('result' => 1)));
        echo json_encode($data);
    }
}

//change_band_name
public function change_band_name() {
  if ($this->ion_auth->logged_in()){
    $user_info = $this->ion_auth->user()->row();
    $band_name = $_POST['band_name'];
    //log_message('error', $band_name);
    if ($band_name == "") {

    } else {
      $band_url = $band_name;
      $s = str_replace("Š","s",$band_url);
      $s = str_replace("š","s",$s);
      $s = str_replace("ć","c",$s);
      $s = str_replace("Ć","c",$s);
      $s = str_replace("Č","c",$s);
      $s = str_replace("č","c",$s);
      $s = str_replace("ž","z",$s);
      $s = str_replace("Ž","z",$s);
      $s = str_replace("đ","dj",$s);
      $s = str_replace("Đ","dj",$s);
      $band_url = preg_replace('/\W+/', '_', strtolower($s));
      $data = array(
        'user_id' => $user_info->id,
        'band_name' => $band_name,
        'band_url' => $band_url
      );
      $this->Ajax_model->change_band_name($data);
    }
  }
}


//upload profile image

public function uploadProfile() {

    $user_info = $this->ion_auth->user()->row();
    $upload_data = array(
        'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/files/profile",
        'upload_url' => base_url() . "/files",
        'allowed_types' => "gif|jpg|png|jpeg",
        'overwrite' => TRUE,
        'max_size' => "8048KB",
        'width' => 400,
        'height' => 1,
        'quality' => "100%",
        'file_name' => microtime(true) . rand(1, 10000)  . '.jpg'
    );
    $file_name = null;
    $this->load->library('upload', $upload_data);
    if ($this->upload->do_upload('userfile')) {
        $uploaded_file = $this->upload->data();
        $file_name = $uploaded_file['file_name'];
        $image_config["image_library"] = "gd2";
        $image_config["source_image"] = $upload_data["upload_path"] . "/" . $file_name;
        $image_config['create_thumb'] = FALSE;
        $image_config['maintain_ratio'] = TRUE;
        $image_config['new_image'] = $upload_data["upload_path"] . "/" . $file_name;
        $image_config['quality'] = "100%";
        $image_config['width'] = 400;
        $image_config['height'] = 1;
        $dim = (intval($upload_data["width"]) / intval($upload_data["height"])) - ($image_config['width'] / $image_config['height']);
        $image_config['master_dim'] = ($dim > 0) ? "height" : "width";
        $this->load->library('image_lib');
        $this->image_lib->initialize($image_config);
        if (!$this->image_lib->resize()) { //Resize image
        }
    }
    if ($file_name == "") {
        //$file_name = 'listing-default.png';
        $data = array(
          'user_id' => $user_info->id,
          'band_profile_image' => $file_name
        );
        $this->Ajax_model->create_profile_image($data);
        // $this->output
        //         ->set_content_type('application/json')
        //         ->set_output(json_encode(array('result' => 0)));
        echo json_encode($data);
    } else {

        $data = array(
            'user_id' => $user_info->id,
            'band_profile_image' => $file_name
        );
        $this->Ajax_model->create_profile_image($data);
        // $this->output
        //         ->set_content_type('application/json')
        //         ->set_output(json_encode(array('result' => 1)));
        echo json_encode($data);
    }
}

public function create_personal_information() {

  if ($this->ion_auth->logged_in()){
     $user_info = $this->ion_auth->user()->row();
     $data = array(
         'user_id' => $user_info->id,
         'city' => $this->input->post('city'),
         'phone' => $this->input->post('phone'),
         'email' => $this->input->post('email'),
         'price_from' => $this->input->post('price_from'),
         'price_to' => $this->input->post('price_to')
     );
     $this->Ajax_model->create_personal_information($data);

   }
   else {
     redirect('auth/login', 'refresh');
   }
 }

 public function create_parties() {

   if ($this->ion_auth->logged_in()){
      $user_info = $this->ion_auth->user()->row();
      $band_id = $this->Ajax_model->get_band_id($user_info->id);
      //delete all rows
      $user_id = $user_info->id;
      $this->Ajax_model->delete_parties($band_id->id);
      //create new rows
      $parties = $this->input->post();
      foreach ($parties['data'] as $party) {

        $data = array(
            'user_id' => $user_info->id,
            'band_id' => $band_id->id,
            'party_id' => $party
        );
        $this->Ajax_model->create_parties($data);
      };

    }
    else {
      redirect('auth/login', 'refresh');
    }
  }

  public function create_genres() {

    if ($this->ion_auth->logged_in()){
       $user_info = $this->ion_auth->user()->row();
       $band_id = $this->Ajax_model->get_band_id($user_info->id);
       //delete all rows
       $user_id = $user_info->id;
       $this->Ajax_model->delete_genres($band_id->id);
       //create new rows
       $genres = $this->input->post();
       foreach ($genres['data'] as $genre) {

         $data = array(
             'user_id' => $user_info->id,
             'band_id' => $band_id->id,
             'genre_id' => $genre
         );
         $this->Ajax_model->create_genres($data);
       };

     }
     else {
       redirect('auth/login', 'refresh');
     }
   }

 public function create_band_type() {

   if ($this->ion_auth->logged_in()){
      $user_info = $this->ion_auth->user()->row();
      $band_id = $this->Ajax_model->get_band_id($user_info->id);
      //delete all rows
      $user_id = $user_info->id;
      $this->Ajax_model->delete_band_type($band_id->id);
      //create new rows
      $type_id = $this->input->post(data);

      $data = array(
          'user_id' => $user_info->id,
          'band_id' => $band_id->id,
          'type_id' => $type_id
      );
      $this->Ajax_model->create_band_type($data);
    }
    else {
      redirect('auth/login', 'refresh');
    }
  }

public function create_instruments() {

  if ($this->ion_auth->logged_in()){
     $user_info = $this->ion_auth->user()->row();
     $band_id = $this->Ajax_model->get_band_id($user_info->id);
     //delete all rows
     $user_id = $user_info->id;
     $this->Ajax_model->delete_instruments($band_id->id);
     //create new rows
     $instruments = $this->input->post();
     foreach ($instruments['data'] as $instrument) {
       $data = array(
           'user_id' => $user_info->id,
           'band_id' => $band_id->id,
           'instrument_id' => $instrument
       );
       $this->Ajax_model->create_instruments($data);
     };
   }
   else {
     redirect('auth/login', 'refresh');
   }
 }

 public function create_description() {

   if ($this->ion_auth->logged_in()){
      $user_info = $this->ion_auth->user()->row();
      $band_id = $this->Ajax_model->get_band_id($user_info->id);
      $user_id = $user_info->id;
      $description = $this->input->post(data);
      $data = array(
            'user_id' => $user_info->id,
            'band_id' => $band_id->id,
            'description' => $description
      );
      $this->Ajax_model->create_description($data);
      }
    else {
      redirect('auth/login', 'refresh');
    }
  }


  public function uploadphotos() {
      $user_info = $this->ion_auth->user()->row();
      $band_id = $this->Ajax_model->get_band_id($user_info->id);
      $upload_data = array(
          'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/files/photos",
          'upload_url' => base_url() . "/files",
          'allowed_types' => "gif|jpg|png|jpeg",
          'overwrite' => TRUE,
          'max_size' => "8048KB",
          'width' => 200,
          'height' => 1,
          'quality' => "100%",
          'file_name' => microtime(true) . rand(1, 10000)  . '.jpg'
      );
      $file_name = null;
      $this->load->library('upload', $upload_data);
      if ($this->upload->do_upload('userfile')) {
          $uploaded_file = $this->upload->data();
          $w = $uploaded_file['image_width']; // original image's width
          $h = $uploaded_file['image_height']; // original images's height
          $file_name = $uploaded_file['file_name'];
          $image_config["image_library"] = "gd2";
          $image_config["source_image"] = $upload_data["upload_path"] . "/" . $file_name;
          $image_config['create_thumb'] = FALSE;
          $image_config['maintain_ratio'] = TRUE;
          $image_config['new_image'] = $upload_data["upload_path"] . "/" . $file_name;
          $image_config['quality'] = "100%";
          if ($w < 1200) {
            $image_config['width'] = $w;
          } else {
          $image_config['width'] = 1200;
          };
          $image_config['height'] = 1;
          $dim = (intval($upload_data["width"]) / intval($upload_data["height"])) - ($image_config['width'] / $image_config['height']);
          $image_config['master_dim'] = ($dim > 0) ? "height" : "width";
          $this->load->library('image_lib');
          $this->image_lib->initialize($image_config);
          if (!$this->image_lib->resize()) { //Resize image
          }
      }
      if ($file_name == "") {
          //$file_name = 'listing-default.png';
          $data = array(
            'user_id' => $user_info->id,
            'photo' => $file_name
          );
          $this->Ajax_model->create_photos($data);
          // $this->output
          //         ->set_content_type('application/json')
          //         ->set_output(json_encode(array('result' => 0)));
          echo json_encode($data);
      } else {

          $data = array(
              'user_id' => $user_info->id,
              'band_id' => $band_id->id,
              'photo' => $file_name
          );
          $insert_id = $this->Ajax_model->create_photos($data);
          if (!empty($insert_id)) {
            $data["photo_id"] = $insert_id;
          };
          // $this->output
          //         ->set_content_type('application/json')
          //         ->set_output(json_encode(array('result' => 1)));
          echo json_encode($data);
      }
  }

  public function deletePhoto() {
    $photo_id = $this->input->post('photo_id');
    $photo_name = $this->input->post('photo_name');

    if (!empty($photo_id)) {
      $this->Ajax_model->delete_photo($photo_id);
    };
    if (!empty($photo_name)) {
      $photo_name =  $_SERVER['DOCUMENT_ROOT']."/files/photos/".$photo_name;
      if (!unlink($photo_name)) {
        $message="Slika nije uspešno obrisana";
        }
        else {
          $message="Slika je uspešno obrisana";
        }
        echo json_encode($message);
    };
  }

   public function create_youtube_link() {

     if ($this->ion_auth->logged_in()){
        $user_info = $this->ion_auth->user()->row();
        $band_id = $this->Ajax_model->get_band_id($user_info->id);
        $user_id = $user_info->id;
        //delete all rows
        //$this->Ajax_model->delete_youtube($user_id);

        $data = $this->input->post();
         foreach ($data as $key => $value) {
           $youtube_id = substr($value, -11);
           $data = array(
              'user_id' => $user_id,
              'band_id' => $band_id->id,
              'youtube_id' => $youtube_id
           );
           //$this->Ajax_model->create_youtube($data);
           $insert_id = $this->Ajax_model->create_youtube($data);
           if (!empty($insert_id)) {
             $data["youtube_id"] = $insert_id;
             echo json_encode($data);
           };
         };
        }

      else {
        redirect('auth/login', 'refresh');
      }
    }


    public function delete_youtube_link() {
      $youtube_id = $this->input->post('youtube_id');
      log_message('error', $youtube_id);
      if (!empty($youtube_id)) {
        $this->Ajax_model->delete_youtube_link($youtube_id);
          $message="Youtube link je uspešno obrisan";
          echo json_encode($message);
      };
    }



}
