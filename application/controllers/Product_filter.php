<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_filter extends CI_Controller {

 public function __construct()
 {
  parent::__construct();
  $this->load->model('Product_filter_model');
  $this->load->model('Profile_model');
 }

 function index()
 {
  $this->data['title'] =  "Pronađi muziku | Bendovi za svadbe";
  $data['parties_list'] = $this->Profile_model->get_parties_list();
  $data['genres_list'] = $this->Profile_model->get_genres_list();
  $data['bands_type_list'] = $this->Profile_model->get_bands_type_list();
  $data['instruments_list'] = $this->Profile_model->get_instruments_list();
  $this->_render_page('template/header', $this->data);
  $this->_render_page('template/navbar');

  $this->_render_page('product_filter', $data);
  $this->_render_page('template/footer');
  //$this->load->view('product_filter', $data);
 }

 function test()
 {
   $minimum_price = "1000";
   $maximum_price = "5000";
   $searchquery = "beog";
   $test = $this->Product_filter_model->test($minimum_price, $maximum_price, $searchquery);
   echo json_encode($test);
 }

 function fetch_data()
 {
  sleep(1);
  $minimum_price = $this->input->post('minimum_price');
  $maximum_price = $this->input->post('maximum_price');
  $band_type = $this->input->post('band');
  $genre = $this->input->post('genre');
  $partie = $this->input->post('partie');
	$searchquery = $this->input->post('searchquery');
  $this->load->library('pagination');
  $config = array();
  $config['base_url'] = '#';
  $config['total_rows'] = $this->Product_filter_model->count_all($minimum_price, $maximum_price, $band_type, $genre, $partie, $searchquery);
  $config['per_page'] = 6;
  $config['uri_segment'] = 3;
  $config['use_page_numbers'] = TRUE;
  $config['full_tag_open'] = '<ul class="pagination">';
  $config['full_tag_close'] = '</ul>';
  $config['first_tag_open'] = '<li>';
  $config['first_tag_close'] = '</li>';
  $config['last_tag_open'] = '<li>';
  $config['last_tag_close'] = '</li>';
  $config['next_link'] = '<i class="material-icons">chevron_right</i>';
  $config['next_tag_open'] = '<li>';
  $config['next_tag_close'] = '</li>';
  $config['prev_link'] = '<i class="material-icons">chevron_left</i>';
  $config['prev_tag_open'] = '<li>';
  $config['prev_tag_close'] = '</li>';
  $config['cur_tag_open'] = "<li class='active'><a href='#'>";
  $config['cur_tag_close'] = '</a></li>';
  $config['num_tag_open'] = '<li class="waves-effect">';
  $config['num_tag_close'] = '</li>';
  $config['num_links'] = 3;
  $this->pagination->initialize($config);
  $page = $this->uri->segment(3);
  $start = ($page - 1) * $config['per_page'];
  $output = array(
   'pagination_link'  => $this->pagination->create_links(),
   'product_list'   => $this->Product_filter_model->fetch_data($config["per_page"], $start, $minimum_price, $maximum_price, $band_type, $genre, $partie, $searchquery)
  );
  echo json_encode($output);
 }

 public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
 {

   $this->viewdata = (empty($data)) ? $this->data : $data;

   $view_html = $this->load->view($view, $this->viewdata, $returnhtml);

   // This will return html on 3rd argument being true
   if ($returnhtml)
   {
     return $view_html;
   }
 }

}
?>
