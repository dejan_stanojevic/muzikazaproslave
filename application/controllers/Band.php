<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Band extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		//$this->load->library(['ion_auth']);
		$this->load->model('Band_model');
		$this->load->helper(['url', 'language']);

	}

	public function index()
	{


			redirect('welcome');
			//$result = $this->Band_model->show_series_genre($band_url);


      $this->_render_page('template/header', $this->data);
			$this->_render_page('template/navbar');

			$this->_render_page('band', $data);
			$this->_render_page('template/footer');

	}

	public function profile()
	{$this->lang->load('auth');
	if ($this->ion_auth->logged_in()){

		 $user_info = $this->ion_auth->user()->row();

		 $this->data['user_info'] = array(
			 'user_id' => $user_info->id,
			 'first_name' => $user_info->first_name,
			 'last_name' => $user_info->last_name,
			 'email' => $user_info->email,
			 'username' => $user_info->username,
			 'last_login' => $user_info->last_login,
			 'profile_url' => $user_info->profile_url,
			 'picture_url' => $user_info->picture_url,
		 );
	 }

			$band_url = $this->uri->segment(3);
			$band_id = $this->Band_model->get_band_id_by_url($band_url);
			if ( !isset($band_id)) {
				redirect('welcome');
			}

			$band_id = $band_id->id;
			$data['band_id'] = $band_id;
			$this->data['title'] =  $this->Band_model->get_band_details($band_id)->band_name;
			$data['band_details'] = $this->Band_model->get_band_details($band_id);
			$data['band_have_parties'] = $this->Band_model->get_band_have_parties($band_id);
			$data['band_have_genres'] = $this->Band_model->get_band_have_genres($band_id);
			$data['band_have_types'] = $this->Band_model->get_band_have_types($band_id);
			$data['band_have_instruments'] = $this->Band_model->get_band_have_instruments($band_id);
			$data['band_have_photos'] = $this->Band_model->get_band_have_photos($band_id);
			$data['band_have_youtubes'] = $this->Band_model->get_band_have_youtubes($band_id);
			$data['band_have_events'] = $this->Band_model->get_band_have_events($band_id);
			$this->_render_page('template/header', $this->data);
			$this->_render_page('template/navbar');

			$this->_render_page('band', $data);
			$this->_render_page('template/footer');

	}

	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
