<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div id="headerProfile" class="valign-wrapper" style=" background-image: url('/files/header/<?php if (empty($band_details->band_cover_image)) { echo ('header_template.jpg');} else {echo ($band_details->band_cover_image);} ?> ')">

  <h1 id="profileName" ><?php if (empty($band_details->band_name)) { echo ('Naziv Grupe');} else {echo ($band_details->band_name);} ?></h1>
</div>

<div id="headerProfileImage">
 <img id="profilePic" class="z-depth-3" src="/files/profile/<?php if (empty($band_details->band_profile_image)) { echo ('profile_template.jpg');} else {echo ($band_details->band_profile_image);} ?> "/>
</div>

<div class="section no-pad-bot">
  <div class="container">
      <div class="row">
        <div class="col s12">
          <div class="divider"></div>
        </div>
          <div class="col s12 center-align"><h5>Osnovne informacije:</h5></div>

          <div id="showPersonalInfo" class="col s12">
            <div class="row">
              <div class="col s6 m3 l3"> <i class="material-icons left">place</i> <?php if (!empty($band_details->city)) { echo $band_details->city;} ?></div>
              <div class="col s6 m3 l3"> <i class="material-icons left">phone</i> <?php if (!empty($band_details->phone)) { echo $band_details->phone;} ?></div>
              <div class="col s6 m3 l3"> <i class="material-icons left">email</i> <?php if (!empty($band_details->email)) { echo $band_details->email;} ?></div>
              <div class="col s6 m3 l3"> <i class="material-icons left">euro_symbol</i> <?php if (!empty($band_details->price_from)) { echo $band_details->price_from; } ?> - <?php  if (!empty($band_details->price_to)) { echo $band_details->price_to; } ?></div>
            </div>
          </div>

          <div class="col s12">
            <div class="divider"></div>
          </div>
            <div class="col s6 center-align">
              <h5 >Vrsta proslave:</h5>
                <?php
                  if (isset($band_have_parties)){
                  foreach ($band_have_parties as $band_have_partie) {
                    echo ($band_have_partie->party_name)." ";
                  } }?>
            </div>

            <div class="col s6 center-align">
              <h5>Žanr muzike:</h5>
              <?php
                if (isset($band_have_genres)){
                foreach ($band_have_genres as $band_have_genre) {
                  echo ($band_have_genre->genre_name)." ";
                } }?>
              <div class="clearfix"></div>

            </div>
            <div class="col s12">
              <div class="divider"></div>
            </div>

            <div class="col s6 center-align">
              <h5>Vrsta grupe:</h5>

              <?php
                if (isset($band_have_types)){
                foreach ($band_have_types as $band_have_type) {
                  echo ($band_have_type->band_type_name)." ";
                } }?>
            </div>

            <div class="col s6 center-align">
              <h5>Instrumenti:</h5>
                <?php
                if (isset($band_have_types)){
                    foreach ($band_have_instruments as $band_instrument) {
                echo ($band_instrument->instrument_name)." ";
              } }?>
            </div>
            <div class="col s12">
              <div class="divider"></div>
            </div>

            <div class="col s12">
              <h5 class="center-align">Kratak opis:</h5>
              <?php
                if (!empty($band_details->description)) {
                  echo $band_details->description;
                }
                else { }
              ?>

            </div>
            <div class="col s12">
              <div class="divider"></div>
            </div>

            <div class="col s12">
              <h5 class="center-align">Fotografije:</h5>
              <div id="photos" class="owl-carousel popup-gallery">
              <?php
              if (!empty($band_have_photos)) {
                  foreach ($band_have_photos as $band_have_photo) { ?>
                    <div class="gallery-item">
                      <a href="/files/photos/<?php echo ($band_have_photo->photo); ?>" data-effect="mfp-zoom-in" title="">
                        <img alt="<?php echo ($band_details->band_name) ?>" src="/files/photos/<?php echo ($band_have_photo->photo); ?>">
                      </a>
                    </div>
                <?php }
              } ?>
            </div>
            <div class="clearfix"></div>
            </div>
            <div class="col s12">
              <div class="divider"></div>
            </div>
            <div class=" col s12">
              <h5 class="center-align">Youtube video:</h5>
              <div class="row">
                <?php
                if (!empty($band_have_youtubes)) {
                    foreach ($band_have_youtubes as $band_have_youtube) { ?>
                    <div class="col s6 m3 mt-4 ytBox" id="yt<?php echo ($band_have_youtube->id); ?>">
                      <div class="videoWrapper">
                      <iframe width="560" height="349" src="http://www.youtube.com/embed/<?php echo ($band_have_youtube->youtube_id); ?>?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
                      </div>
                    </div>
                  <?php }
                } ?>
              </div>
            </div>
            <div class="col s12">
              <div class="divider"></div>
            </div>
            <div class="col s12">
              <h5>Kalendar:</h5>
              <div id="datepicker_container"></div>
                <input type="text" id="band_datepicker">


            </div>
        </div>
    </div>
  </div>



<script>


$(document).ready(function(){

  var container = $('#datepicker_container');
  $('#band_datepicker').datepicker({
    firstDay: 1,
    container: container,
    minDate: new Date(),
    onOpenStart: function() {
      console.log('onOpenEnd');
    }
  });

//setTimeout("$('#band_datepicker').datepicker('open')",1000);


});


</script>
