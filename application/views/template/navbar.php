<div class="top-nav">
  <div class="container">
    <!-- Dropdown Structure -->
    <ul id="usermenu" class="dropdown-content">
      <li><a href="<?php echo base_url('profile'); ?>">Profile</a></li>
      <li class="divider"></li>
      <li><a href="<?php echo base_url('auth/logout'); ?>">Odjava</a></li>
    </ul>
    <ul class="right">
    <?php 	if ($this->ion_auth->logged_in()){ ?>
      <li><a class="dropdown-trigger" href="#!" data-target="usermenu"><?php echo $user_info['first_name']; ?><i class="material-icons right">arrow_drop_down</i></a></li>

      <?php
    } else {
    ?>
    <li><a href="<?php echo base_url('auth/login'); ?>"> <i class="fa fa-sign-in fa"></i> Prijava</a></li>
    <li><a href="<?php echo base_url('auth/register'); ?>"> <i class="fa fa-user-plus fa" ></i> Registracija</a></li>
    <?php } ?>
    </ul>
  </div>
</div>
<nav class="">
  <div class="nav-wrapper container">
    <a href="/" class="brand-logo" alt="Muzika Za Proslave"><img src="/assets/images/logo-white.svg"</a>
    <ul class="right hide-on-med-and-down">
      <li><a href="/">Početna</a></li>
      <li><a href="/findMusic">Pronađi bend</a></li>
      <li><a href="/zvezdeGranda">Zvezde granda</a></li>
      <!--
      <?php 	if ($this->ion_auth->logged_in()){ ?>
        <li><?php echo $user_info['first_name']; ?> </li>
        <li><a href="<?php echo base_url('auth/logout'); ?>"> <i class="fa fa-sign-in fa-2"></i> Logout</a></li>
        <?php
      } else {
      ?>

      <li><a href="<?php echo base_url('auth/register'); ?>"> <i class="fa fa-user-plus fa-2" ></i> Sign Up</a></li>
      <li><a href="<?php echo base_url('auth/login'); ?>"> <i class="fa fa-sign-in fa-2"></i> Login</a></li>
      <?php } ?>
    -->
    </ul>
    <ul id="nav-mobile" class="sidenav">
      <li><a href="#">Početna</a></li>
      <li><a href="#">Pronađi bend</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
  </div>
</nav>
<main>
