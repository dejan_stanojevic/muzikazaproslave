<header>
  <nav role="navigation">
    <div class="nav-wrapper">
    	<a id="logo-container" href="<?php echo base_url();?>" class="brand-logo center logo-container">
    		<img src="/assets/images/logo-white.svg" border="0">
    	</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="javascript:history.go(-1)" class="<?php echo config_item('heading_color_text');?>-text"><i class="fa fa-arrow-left fa-2 <?php echo config_item('heading_color_text');?>-text"></i> <?php echo lang('back');?></a></li>
      </ul>
      <ul id="nav-mobile" class="side-nav">
		    <li class="center-align nav-logo">
				    	<a href="<?php echo base_url();?>" class="logo-container">
				    		<img src="<?php echo $site_logo;?>" border="0">
				    	</a>
		    </li>
		    <li><div class="divider"></div></li>

      </ul>

    </div>
  </nav>
</header>
<main>
