<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div id="headerProfile" class="valign-wrapper" style=" background-image: url('/files/header/<?php if (empty($band_details->band_cover_image)) { echo ('header_template.jpg');} else {echo ($band_details->band_cover_image);} ?> ')">
  <input id="uploadHeaderBackground"  name="headerBackground" type="file" style="display:none">
  <a id="profileBackground" class="btn-floating btn-small waves-effect waves-light tooltipped" data-position="right" data-tooltip="Promeni pozadinsku sliku profila"><i class="material-icons">edit</i></a>
  <h1 contenteditable="true" id="profileName" class="tooltipped" data-position="top" data-tooltip="Klikni i promeni naziv grupe"><?php if (empty($band_details->band_name)) { echo ('Naziv Grupe');} else {echo ($band_details->band_name);} ?></h1>
</div>

<div class="progress mt-4">
  <div class="determinate" style="width: 0%"></div>
</div>

<div id="headerProfileImage">
  <input id="uploadProfileImage"  name="profileImage" type="file" style="display:none">
 <img id="profilePic" class="z-depth-3" src="/files/profile/<?php if (empty($band_details->band_profile_image)) { echo ('profile_template.jpg');} else {echo ($band_details->band_profile_image);} ?> "/>
 <a id="profileImage" class="btn-floating btn-small waves-effect waves-light tooltipped" data-position="right" data-tooltip="Promeni profilnu sliku"><i class="material-icons">edit</i></a>
</div>

<div class="section no-pad-bot">
  <div class="container">
    <div id="editProfile" class="section">
      <div class="row">
        <div class="col s12">
          <div class="divider"></div>
        </div>
          <div class="col s12 "><h5>Osnovne informacije:</h5></div>

          <div id="showPersonalInfo" class="col s12">
            <a id="editPersonalInfo" onclick="editPersonalInfo()" class="btn-floating btn-small waves-effect waves-light tooltipped" data-position="right" data-tooltip="Promeni osnovne informacije"><i class="material-icons">edit</i></a>
            <div class="row">
              <div class="col s12 m6 l3"> <i class="material-icons left">place</i> <?php if (!empty($band_details->city)) { echo $band_details->city;} ?></div>
              <div class="col s12 m6 l3"> <i class="material-icons left">phone</i> <?php if (!empty($band_details->phone)) { echo $band_details->phone;} ?></div>
              <div class="col s12 m6 l3"> <i class="material-icons left">email</i> <?php if (!empty($band_details->email)) { echo $band_details->email;} ?></div>
              <div class="col s12 m6 l3"> <i class="material-icons left">euro_symbol</i> <?php if (!empty($band_details->price_from)) { echo $band_details->price_from; } ?> - <?php  if (!empty($band_details->price_to)) { echo $band_details->price_to; } ?></div>
            </div>
          </div>
          <form id="formPersonalInfo">
            <div class="input-field col s12 m4 l2">
              <input name="city" id="city" type="text" class="validate">
              <label for="city">Grad</label>
            </div>
            <div class="input-field col s12 m4 l3">
              <input name="phone" id="phone" type="text" class="validate">
              <label for="phone">Telefon</label>
            </div>
            <div class="input-field col s12 m4 l3">
              <input name="email" id="email" type="text" class="validate">
              <label for="email">Email</label>
            </div>

            <div class="input-field col s6 m3 l2">
              <input  name="price_from" id="price_from" type="number" class="validate">
              <label for="price_from">Cena od (€):</label>
            </div>

            <div class="input-field col s6 m3 l2">
              <input  name="price_to" id="price_to" type="number" class="validate">
              <label for="price_to">Cena do (€):</label>
            </div>
            <div class="col s12 mt-4">
              <button id="submitPersonalInfo" class="btn waves-effect " type="submit" name="action">Sačuvaj<i class="material-icons right">send</i></button>
            </div>
          </form>
          <div class="col s12">
            <div class="divider"></div>
          </div>
            <div class="col s12">
              <h5 >Vrsta proslave:</h5>
                <form id="formParties">
                <?php
                foreach ($parties_list as $party) { ?>
                  <label class="col s6 m4 l3">
                    <input type="checkbox" id="party_id_<?php echo $party->id ?>" name="party" value="<?php echo $party->id ?>"
                    <?php
                        foreach ($band_have_parties as $band_have_partie) {
                          if ($party->id == $band_have_partie->id) {
                      ?>  checked <?php
                    } }?>
                    />
                    <span> <?php echo $party->party_name ?> </span>
                  </label>
                <?php }
                 ?>
                <div class="clearfix"></div>
                <button id="submitParties" class="btn waves-effect " type="submit" name="action">Sačuvaj<i class="material-icons right">send</i></button>
              </form>
            </div>
            <div class="col s12">
              <div class="divider"></div>
            </div>
            <div class="col s12">
              <h5>Žanr muzike:</h5>
              <form id="formGenre">
              <?php foreach ($genres_list as $genre) { ?>
                <label  class="col s6 m4 l3">
                  <input type="checkbox" id="genre_id_<?php echo $genre->id ?>" name="genre" value="<?php echo $genre->id ?>"
                  <?php
                      foreach ($band_have_genres as $band_have_genre) {
                        if ($genre->id == $band_have_genre->id) {
                    ?>  checked <?php
                  } }?>
                  />
                  <span> <?php echo $genre->genre_name ?> </span>
                </label>
              <?php } ?>
              <div class="clearfix"></div>
              <button id="submitGenres" class="btn waves-effect " type="submit" name="action">Sačuvaj<i class="material-icons right">send</i></button>
            </form>
            </div>
            <div class="col s12">
              <div class="divider"></div>
            </div>

            <div class="col s12">
              <h5>Vrsta grupe:</h5>
              <form id="formBandType">
                <?php foreach ($bands_type_list as $bands_type) { ?>
                <label class="col s12 m6 l3">
                  <input id="bands_type_id_<?php echo $bands_type->id ?>" name="bands_type" value="<?php echo $bands_type->id ?>" type="radio" class="validate" data-error=".errorTxtbands_type"
                  <?php
                      foreach ($band_have_types as $band_have_type) {
                        if ($bands_type->id == $band_have_type->id) {
                    ?>  checked <?php
                  } }?>
                  >
                  <span><?php echo $bands_type->band_type_name ?></span>
                </label>
                <?php } ?>
                <button id="submitBandType" class="btn waves-effect " type="submit" name="action">Sačuvaj<i class="material-icons right">send</i></button>
              </form>
            </div>
            <div class="col s12">
              <div class="divider"></div>
            </div>

            <div class="col s12">
              <h5>Instrumenti:</h5>
              <form id="formInstruments">
                <?php foreach ($instruments_list as $instrument) { ?>
                  <label class="col s6 m3">
                    <input type="checkbox" id="instrument_id_<?php echo $instrument->id ?>" name="instrument" value="<?php echo $instrument->id ?>"
                    <?php
                        foreach ($band_have_instruments as $band_instrument) {
                          if ($instrument->id == $band_instrument->id) {
                      ?>  checked <?php
                    } }?>
                    />
                    <span> <?php echo $instrument->instrument_name ?> </span>
                  </label>
                <?php } ?>
                <button id="submitInstruments" class="btn waves-effect " type="submit" name="action">Sačuvaj<i class="material-icons right">send</i></button>
              </form>
            </div>
            <div class="col s12">
              <div class="divider"></div>
            </div>

            <div class="col s12">
              <h5>Kratak opis:</h5>
              <!-- <?php
                if (!empty($band_details->description)) {
                  echo $band_details->description;
                }
                else {
                  echo ('prazno');
                }
              ?> -->
              <textarea placeholder="Unesite kratak opis" name="description" id="description" class=""></textarea>
              <button id="submitDescription" class="btn waves-effect " type="submit" name="action">Sačuvaj<i class="material-icons right">send</i></button>
            </div>
            <div class="col s12">
              <div class="divider"></div>
            </div>

            <div class="col s12">
              <h5>Fotografije:</h5>
              <div class="image_list">
              <?php
              if (!empty($band_have_photos)) {

                  foreach ($band_have_photos as $band_have_photo) { ?>
                  <div class="col s6 m3" id="photo<?php echo ($band_have_photo->id); ?>">
                    <a data-photoid="<?php echo ($band_have_photo->id); ?>" data-photoname="<?php echo ($band_have_photo->photo); ?>" class ="btn-floating btn-small red tooltipped removePhoto" data-position="top" data-tooltip="Obriši sliku"><i class="material-icons">close</i></a>
                    <img alt="<?php echo ($band_details->band_name) ?>" src="/files/photos/<?php echo ($band_have_photo->photo); ?>">
                  </div>
                <?php }

              } ?>
            </div>
            <div class="clearfix"></div>
              <div class="file-field input-field">
                <div class="btn">
                  <span>Dodaj fotografiju</span>
                  <input id="uploadPhoto" type="file">
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate" type="text" placeholder="Dodaj fotografiju">
                </div>
              </div>
              <div class="progress mt-4">
                <div class="determinate photo" style="width: 0%"></div>
              </div>
            </div>
            <div class="col s12">
              <div class="divider"></div>
            </div>
            <div class=" col s12">
              <h5>Youtube video:</h5>
              <div class="row youtube_list">
                <?php
                if (!empty($band_have_youtubes)) {

                    foreach ($band_have_youtubes as $band_have_youtube) { ?>
                    <div class="col s6 m3 mt-4 ytBox" id="yt<?php echo ($band_have_youtube->id); ?>">
                      <a data-youtubeid="<?php echo ($band_have_youtube->id); ?>" data-youtubeid="<?php echo ($band_have_youtube->youtube_id); ?>" class ="btn-floating btn-small red tooltipped removeYT" data-position="top" data-tooltip="Obriši Youtube video"><i class="material-icons">close</i></a>
                      <div class="videoWrapper">
                      <iframe width="560" height="349" src="http://www.youtube.com/embed/<?php echo ($band_have_youtube->youtube_id); ?>?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
                      </div>
                    </div>
                  <?php }

                } ?>

              </div>
              <div class="center-align">
                <button id="ytOpen" class="btn waves-effect waves-light" type="submit" >Dodaj Youtube video
                  <i class="material-icons right">slideshow</i>
                </button>
              </div>
            </div>
            <div class="col s12">
              <div class="divider"></div>
            </div>
            <div class="col s12 m6">
              <h5>Kalendar:</h5>
              <input type="hidden" id="band_id" value="<?php echo ($band_details->id); ?>"/>
              <div id="calendar"></div>
            </div>
        </div>
      </div>
    </div>
  </div>
    <!-- Modal Structure -->
  <div id="modalProfile" class="modal">
    <div class="modal-content">
      <div class="modal-icon center"></div>
      <h4 class="center"></h4>
      <div class="message center"></div>
  </div>
  <div class="modal-footer center">
    <a href="/profile" class="modal-close waves-effect btn">Nastavi</a>
  </div>
</div>

<div id="modalYTlink" class="modal">
  <div class="modal-content">
    <div class="modal-icon center"></div>
    <h4 class="center">Dodaj Youtube link</h4>
    <div class=" center">
      <div class="input-field col s12">
        <form id="modalYoutube">
          <input placeholder="Youtube link" name="youtubelink" id="youtubelink" type="text" class="validate">
          <button id ="submitYTlink" type="submit" class="waves-effect waves-light btn"><i class="material-icons left">send</i>Sačuvaj</button>
        </form>
      </div>
    </div>
  </div>
</div>

<script>

var jsonBandDetails = <?php echo(json_encode($band_details)) ?>;
  if (jsonBandDetails.city) {
    document.getElementById("formPersonalInfo").style.display = "none";
    var keys = Object.keys(jsonBandDetails);
    keys.forEach(function(key){
      if (key == "city"){
        document.getElementById(key).value = jsonBandDetails[key];
      }
      if (key == "phone"){
        document.getElementById(key).value = jsonBandDetails[key];
      }
      if (key == "email"){
        document.getElementById(key).value = jsonBandDetails[key];
      }
      if (key == "price_from"){
        document.getElementById(key).value = jsonBandDetails[key];
      }
      if (key == "price_to"){
        document.getElementById(key).value = jsonBandDetails[key];
      }
      if (key == "description"){
        document.getElementById(key).value = jsonBandDetails[key];
      }
    });
  } else {
  document.getElementById("formPersonalInfo").style.display = "inline-block";
  }


  function editPersonalInfo() {
      document.getElementById("showPersonalInfo").style.display = "none";
      document.getElementById("formPersonalInfo").style.display = "inline-block";
  }



  //checkboxes state changed submit

  const checkboxes = ["formParties ", "formGenre ", "formInstruments "];
  checkboxes.forEach(function(checkbox){
    $("#" + checkbox +" input:checkbox").each(function() {
      $(this).parents('form').find(':submit').prop('disabled',true);
      $(this).change(function()
      {
        $(this).parents('form').find(':submit').prop('disabled',false);
      });
    });
  });

  //radio button state changed submit

  const radiobuttons = ["formBandType "];
  radiobuttons.forEach(function(radiobutton){
    $("#" + radiobutton +" input:radio").each(function() {
      $(this).parents('form').find(':submit').prop('disabled',true);
      $(this).change(function()
      {
        $(this).parents('form').find(':submit').prop('disabled',false);
      });
    });
  });

  //input:text state changed submit

  const textinputs = ["formYoutube "];
  textinputs.forEach(function(textinput){
    $("#" + textinput +" input:text").each(function() {
      $(this).parents('form').find(':submit').prop('disabled',true);
      $(this).change(function()
      {
        $(this).parents('form').find(':submit').prop('disabled',false);
      });
    });
  });


</script>
