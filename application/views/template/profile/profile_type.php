<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

  <div class="section no-pad-bot" id="index-banner">
      <div class="container">
        <br><br>


    <?php
    if ( !isset($profile_type)){ ?>

      <h1 class="header center orange-text">Kreiraj Profil</h1>
      <div class="row center">
        <h5 class="header col s12 light">Odaberite tip profila koji želite da kreirate</h5>
      </div>
      <div class="section">
          <!--   Icon Section   -->
          <div class="row">
            <?php
            foreach ($profile_list as $profile)
                { ?>

            <div class="col s12 m4">
              <div class="icon-block center">
                <h5 class="center"><?php  echo ($profile->profile_type) ?></h5>
                <button data-profile="<?php  echo ($profile->id) ?>" data-profiletype="<?php  echo ($profile->profile_type) ?>"class="btn waves-effect waves-light create_user_profile_type" name="action">
                  <i class="material-icons right">send</i>Kreiraj profil
                </button>
                <p class="light"><?php  echo ($profile->profile_desc) ?></p>
              </div>
            </div>
            <?php }?>
            </div>
          </div>
      </div>
  <?php } else {
    echo ('postoji kreiran profil');
    //var_dump($profile_type);
  }
    ?>
    <!-- Modal Structure -->
    <div id="modalProfile" class="modal">
      <div class="modal-content">
        <div class="modal-icon center"></div>
        <h4 class="center"></h4>
        <div class="message center"/></div>
      </div>

      <div class="modal-footer center">
         <a href="/profile" class="modal-close waves-effect btn">Nastavi</a>
    </div>
  </div>
