<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $title; ?></title>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link href="<?php echo base_url();?>assets/css/font-awesome/css/all.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/materialize.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/fullcalendar/fullcalendar.css" />
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/fullcalendar/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/fullcalendar/fullcalendar.min.js"></script>
  </head>
  <body>
