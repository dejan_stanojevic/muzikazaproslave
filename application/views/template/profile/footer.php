<!-- Modal Structure -->
<div id="modal" class="modal">
	<div class="modal-content">
		<div class="modal-header">
			<a class="modal-close right waves-effect btn-flat"><i class="material-icons red-text md-24">close</i></a>
		</div>
		<div class="modal-icon center"><i class="large material-icons"></i></div>
		<div class="message center"></div>
		<div class="modal-footer">

    </div>
	</div>
</div>

</main>
<footer class="page-footer">
	<div class="container">
		<div class="row">
			<div class="col l6 s12">
				<h5 class="white-text">Company Bio</h5>
				<p class="grey-text text-lighten-4">We are a team of college students working on this project like it's our full time job. Any amount would help support and continue development on this project and is greatly appreciated.</p>
			</div>
			<div class="col l3 s12">
				<h5 class="white-text">Settings</h5>
				<ul>
					<li><a class="white-text" href="#!">Link 1</a></li>
					<li><a class="white-text" href="#!">Link 2</a></li>
					<li><a class="white-text" href="#!">Link 3</a></li>
					<li><a class="white-text" href="#!">Link 4</a></li>
				</ul>
			</div>
			<div class="col l3 s12">
				<h5 class="white-text">Connect</h5>
				<ul>
					<li><a class="white-text" href="#!">Link 1</a></li>
					<li><a class="white-text" href="#!">Link 2</a></li>
					<li><a class="white-text" href="#!">Link 3</a></li>
					<li><a class="white-text" href="#!">Link 4</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="footer-copyright">
		<div class="container">
		Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
		</div>
	</div>
</footer>
    <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script> -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/materialize.min.js"></script>

		<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>assets/js/additional-methods.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>assets/owlcarousel/owl.carousel.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>assets/magnificpopup/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/profile.js"></script>
  </body>
</html>
