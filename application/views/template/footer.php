<!-- Modal Structure -->
<div id="modal" class="modal">
	<div class="modal-content">
		<div class="modal-header">
			<a class="modal-close right waves-effect btn-flat"><i class="material-icons red-text md-24">close</i></a>
		</div>
		<div class="modal-icon center"><i class="large material-icons"></i></div>
		<div class="message center"></div>
		<div class="modal-footer">

    </div>
	</div>
</div>

</main>
<footer class="page-footer">
	<div class="footer-copyright">
		<div class="container center-align">
		Muzika za proslave
		</div>
	</div>
</footer>
    <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script> -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/materialize.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>assets/js/additional-methods.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>assets/owlcarousel/owl.carousel.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>assets/magnificpopup/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/functions.js"></script>
  </body>
</html>
