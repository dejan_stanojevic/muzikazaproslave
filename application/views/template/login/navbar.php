<style>
.sidenav {
    transform: translateX(0%) !important;
}
section.admin {
  padding-left:300px;
}

.sidenav li > a.active {
  background-color: #eee;
}
</style>


		<ul id="slide-out" class="sidenav">
		    <li class="center-align nav-logo">
				    	<a href="<?php echo base_url();?>" class="logo-container">
				    		<img style="max-width: 90%" src="/assets/images/logo-color.svg"/>
				    	</a>
		    </li>
		    <li><div class="divider"></div></li>
			<li><div class="user-view center">

			      	<a style="margin-top:0;" href="<?php echo base_url('auth/edit_user/'.$this->encryption->encrypt($user_info['user_id']));?>" class="black-text name"><?php echo $user_info['first_name'].' '.$user_info['last_name'];?></a>
			      	<a href="<?php echo base_url('auth/edit_user/'.$this->encryption->encrypt($user_info['user_id']));?>" class="email black-text"><?php echo $user_info['email'];?></a>
			      	Last login: <?php echo date('d/M/Y H:i',$user_info['last_login'])?>
			    </div>
			</li>
		    <li><div class="divider"></div></li>

              <li><a href="<?php echo base_url();?>"><i class="fa fa-home fa-2 "></i> Home</a></li>

              <?php if($this->ion_auth->is_admin()) { ?>
                <li><a href="<?php echo base_url('/auth/recomended_bands');?>"><i class="fa fa-star fa-2 "></i>Preporučeni Bendovi</a></li>
                <li><a href="<?php echo base_url('/auth/zvezde_granda');?>"><i class="fa fa-star fa-2 "></i>Zvezde Granda</a></li>
                <li><div class="divider"></div></li>
              	<li><a href="<?php echo base_url('auth/');?>"><i class="fa fa-user fa-2 "></i> <?php echo lang('index_heading')?></a></li>
              	<li><a href="<?php echo base_url('auth/create_user');?>"><i class="fa fa-user-plus fa-2 "></i> <?php echo lang('create_user_submit_btn')?></a></li>
              	<li><a href="<?php echo base_url('auth/create_group');?>"><i class="fa fa-users fa-2 "></i> <?php echo lang('create_group_submit_btn')?></a></li>
              <?php } ?>


		    <li><div class="divider"></div></li>
		    <li><a href="<?php echo base_url('auth/edit_user/'.$this->encryption->encrypt($user_info['user_id']));?>"><i class="fa fa-edit fa-2 "></i>Uredi profil</a></li>
		    <li><a href="<?php echo base_url('auth/change_password');?>"><i class="fa fa-key fa-2 "></i>Promeni lozinku</a></li>
		    <li><a href="<?php echo base_url('auth/logout');?>"><i class="fa fa-sign-out fa-2 "></i>Odjavi se</a></li>
</ul>
<main>

<script>
var href = window.location.href
$('.sidenav').find('a[href="' + href + '"]').addClass('active');
</script>
