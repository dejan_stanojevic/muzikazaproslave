	<div class="had-container">
		<div class="row">
			<div class="col s12 m12 l6 offset-l3 red center-align valign-wrapper" style="min-height:400px">
					<div class="white-text heading" style="margin:auto;width:100%;">
						<h5><?php echo $heading; ?></h5>
						<?php echo $message; ?>
					</div>
			</div>
		</div>
	</div>