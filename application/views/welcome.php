<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

  <div class="section no-pad-top">
    <div class="hero-image">
      <div class="hero-text">
        <h1>Bendovi za svaku priliku</h1>
        <a class="waves-effect waves-light btn"><i class="material-icons left">search</i>button</a>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="section">
      <div class="row">
        <div class="col s12">
          <h4 class="center-align">Preporučujemo</h4>
          <?php //var_dump ($recomended_list) ?>
          <div class="owl-carousel">

          <?php foreach ($recomended_list as $recomended_band) { ?>
            <div class="card">
              <div class="card-image waves-effect waves-block waves-light">
                <img class="activator" src="files/profile/<?php if( isset( $recomended_band->band_profile_image )) { echo $recomended_band->band_profile_image; };?>">
              </div>
              <div class="card-content">
                <span class="card-title activator grey-text text-darken-4"><?php echo $recomended_band->band_name?><i class="material-icons right">more_vert</i></span>
                <p><a href="/band/profile/<?php echo $recomended_band->band_url?>">Detaljnije</a></p>
              </div>
              <div class="card-reveal">
                <span class="card-title grey-text text-darken-4"><?php echo $recomended_band->band_name?><i class="material-icons right">close</i></span>
                <p><?php echo $recomended_band->description?></p>
                <p><a href="/band/profile/<?php echo $recomended_band->band_url?>">Detaljnije</a></p>
              </div>
            </div>
          <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid home-filter" class="gradient">
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col s12">
            <h1 class="center-align text-shadow">Bendovi za svadbe</h1>
          </div>
        </div>
        <div class="divider light"></div>
        <div class="row">
            <p class="center-align text-shadow filter-title"><i class="material-icons">grade</i>Žanr muzike</p>
          <?php foreach ($genres_list as $genre) { ?>
            <label  class="col s6 m4 l3">
              <input type="checkbox" id="genre_id_<?php echo $genre->id ?>" name="genre" value="<?php echo $genre->id ?>"/>
              <span> <?php echo $genre->genre_name ?> </span>
            </label>
          <?php } ?>
        </div>

        <div class="row">
          <div class="col s12">
            <p class="center-align text-shadow filter-title"><i class="material-icons">euro_symbol</i>Cena</p>
              <input type="text" class="js-range-slider" name="my_range" value="" />
              <input type="hidden" name="priceFrom" id="priceFrom">
              <input type="hidden" name="priceTo" id="priceTo">
          </div>
        </div>

        <div class="row">
          <div class="col s12 m4">
            <p class="center-align text-shadow filter-title"><i class="material-icons">grade</i>Tip proslave</p>
            <div class="input-field">
              <select >
                 <option value="" disabled selected>izaberi</option>
                <?php foreach ($parties_list as $party) { ?>
                  <option id="party_id_<?php echo $party->id ?>" name="party" value="<?php echo $party->id ?>"><?php echo $party->party_name ?></option>
                <?php }?>
              </select>
            </div>
          </div>
          <div class="col s12 m4">
            <p class="center-align text-shadow filter-title"><i class="material-icons">music_video</i>Vrsta grupe</p>
            <div class="input-field">
              <select >
                 <option value="" disabled selected>izaberi</option>
                  <?php foreach ($bands_type_list as $bands_type) { ?>
                  <option id="party_id_<?php echo $bands_type->id ?>" name="party" value="<?php echo $bands_type->id ?>"><?php echo $bands_type->band_type_name ?></option>
                <?php }?>
              </select>
            </div>
          </div>
          <div class="col s12 m4 ">
              <p class="center-align filter-title text-shadow"><i class="material-icons">date_range</i>Datum proslave</p>
              <input type="text" placeholder="Izaberi datum" class="datepicker">
          </div>
          <div class="clearfix"></div>
          <div class="col s12 center-align ">
          <button id="submit" class="btn waves-effect waves-light center-align" type="submit" name="action">PRETRAŽI<i class="material-icons right">send</i></button>
          </div>
        </div>
      </div>
    </div>
  </div>

<script>

  $(".js-range-slider").ionRangeSlider({
      type: "double",
      step: 50,
      min: 0,
      max: 5000,
      from: 0,
      to: 5000,
      grid: true,
      onStart: function (data) {
        priceFrom = data.from;
        priceTo = data.to;
      },
      onFinish: function (data) {
         $('#priceFrom').val(data.from);
         $('#priceTo').val(data.to);
       },
  });
</script>
