
<div class="had-container">
  <div class="row">
    <div class="col s12 m12 l4 offset-l4">
		<div class="card-panel heading center">
      <h1><?php echo lang('create_group_heading');?></h1>
      <p><?php echo lang('create_group_subheading');?></p>
  	</div>

    <div id="infoMessage"><?php echo $message;?></div>

    <?php echo form_open("auth/create_group");?>

      <p class="input-field">
            <?php echo lang('create_group_name_label', 'group_name');?> <br />
            <?php echo form_input($group_name);?>
      </p>

      <p class="input-field">
            <?php echo lang('create_group_desc_label', 'description');?> <br />
            <?php echo form_input($description);?>
      </p>

      <p><button name="submit" type="submit" class="btn  white-text waves-effect waves-light"><i class="fa fa-check left"></i>Kreiraj grupu</button></p>

    <?php echo form_close();?>

    </div>
  </div>
</div>
