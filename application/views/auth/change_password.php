<div class="had-container">
  <div class="row">
    <div class="col s12 m12 l4 offset-l4">
		<div class="card-panel heading center">
      <h1><?php echo lang('change_password_heading');?></h1>
      <p><?php echo lang('create_group_subheading');?></p>
  	</div>



<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/change_password");?>

      <p class="input-field">
            <?php echo lang('change_password_old_password_label', 'old_password');?> <br />
            <?php echo form_input($old_password);?>
      </p>

      <p class="input-field">
            <label for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label> <br />
            <?php echo form_input($new_password);?>
      </p>

      <p class="input-field">
            <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?> <br />
            <?php echo form_input($new_password_confirm);?>
      </p>

      <?php echo form_input($user_id);?>

      <p><button name="submit" type="submit" class="btn  white-text waves-effect waves-light"><i class="fa fa-check left"></i>Promeni lozinku</button></p>

<?php echo form_close();?>

    </div>
  </div>
</div>
