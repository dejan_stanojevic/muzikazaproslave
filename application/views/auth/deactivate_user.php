<div class="had-container">
  <div class="row">
    <div class="col s12 m12 l4 offset-l4">

		<div class="card-panel heading center">
      <h1><?php echo lang('deactivate_heading');?></h1>
      <p><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></p>
  	</div>



<?php echo form_open("auth/deactivate/".$user->id);?>

<p class="input-field">
  	<?php echo lang('deactivate_confirm_y_label', 'confirm');?>
    <input type="radio" name="confirm" value="yes" checked="checked" />
    <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
    <input type="radio" name="confirm" value="no" />
  </p>

  <?php echo form_hidden($csrf); ?>
  <?php echo form_hidden(['id' => $user->id]); ?>

  <p><button name="submit" type="submit" class="btn  white-text waves-effect waves-light"><i class="fa fa-check left"></i>Deaktiviraj korisnika</button></p>

<?php echo form_close();?>
    </div>
  </div>
</div>
