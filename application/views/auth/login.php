<div class="had-container">
  <div class="row">
    <div class="col s12 m12 l4 offset-l4">

		<div class="card-panel heading center">
            	<h5><?php echo lang('login_heading');?></h5>
            	<?php echo lang('login_subheading');?>
  		</div>

      <div id="infoMessage">
      	<?php
      		if($message){
      			echo '<div class="card-panel red white-text heading center">';
      			echo $message;
      			echo '</div>';
      		}
      	?>
      </div>

      <?php echo form_open("", array('class'=>'login-form'));?>

            <p class="input-field">
                  <?php echo form_label(lang('login_identity_label'), 'identity');?>
                  <?php echo form_input($identity);?>
                  <?php echo form_error('identity','<div class="red-text">','</div>'); ?>
            </p>

            <p class="input-field">
                  <?php echo form_label(lang('login_password_label'), 'password');?>
                  <?php echo form_input($password);?>
                  <?php echo form_error('password','<div class="red-text">','</div>'); ?>
            </p>

            <p>
              <label>
                <input type="checkbox" name="remember" id="remember" value="1" />
                <span><?php echo lang('login_remember_label');?></span>
              </label>
            </p>
        <p>
          <button name="submit" type="submit" class="btn  white-text waves-effect waves-light"><i class="fa fa-sign-in white-text left"></i><?php echo lang('login_submit_btn');?></button>
          <a href="<?php echo base_url();?>" class="btn white-text waves-effect waves-light"><i class="fa fa-times white-text left"></i>Odustani</a>
          <a href="<?php echo base_url();?>auth/register" class="btn right waves-effect waves-light">Registracija</a>
        </p>
      <?php echo form_close();?>

      <p class="row">
      	<a href="forgot_password" class="left"><?php echo lang('login_forgot_password');?></a>
      </p>

    </div>
  </div>
</div>
