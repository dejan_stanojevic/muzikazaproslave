<div class="had-container">
  <div class="row">
    <div class="col s12 m12 l4 offset-l4">

		<div class="card-panel heading center">
      <h1><?php echo lang('create_user_heading');?></h1>
      <p><?php echo lang('create_user_subheading');?></p>
  	</div>

    <div id="infoMessage"><?php echo $message;?></div>



<?php echo form_open("auth/create_user");?>

      <p class="input-field">
            <?php echo lang('create_user_fname_label', 'first_name');?> <br />
            <?php echo form_input($first_name);?>
      </p>

      <p class="input-field">
            <?php echo lang('create_user_lname_label', 'last_name');?> <br />
            <?php echo form_input($last_name);?>
      </p>

      <?php
      if($identity_column!=='email') {
          echo '<p>';
          echo lang('create_user_identity_label', 'identity');
          echo '<br />';
          echo form_error('identity');
          echo form_input($identity);
          echo '</p>';
      }
      ?>

      <p class="input-field">
            <?php echo lang('create_user_company_label', 'company');?> <br />
            <?php echo form_input($company);?>
      </p>

      <p class="input-field">
            <?php echo lang('create_user_email_label', 'email');?> <br />
            <?php echo form_input($email);?>
      </p>

      <p class="input-field">
            <?php echo lang('create_user_phone_label', 'phone');?> <br />
            <?php echo form_input($phone);?>
      </p>

      <p class="input-field">
            <?php echo lang('create_user_password_label', 'password');?> <br />
            <?php echo form_input($password);?>
      </p>

      <p class="input-field">
            <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
            <?php echo form_input($password_confirm);?>
      </p>

      <p><button name="submit" type="submit" class="btn  white-text waves-effect waves-light"><i class="fa fa-check left"></i>Kreiraj novog korisnika</button></p>

<?php echo form_close();?>

    </div>
  </div>
</div>
