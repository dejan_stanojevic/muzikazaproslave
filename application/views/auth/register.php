
<div class="had-container">
  <div class="row">
    <div class="col s12 m12 l4 offset-l4">

		<div class="card-panel heading center">
      <h5>Registracija</h5>
      <p>Kreirajte Vaš nalog</p>
  	</div>

      <div id="infoMessage">
      	<?php
      		if($message){
      			echo '<div class="card-panel red white-text heading center">';
      			echo $message;
      			echo '</div>';
      		}
      	?>
      </div>

      <?php echo form_open("auth/register");?>

            <p class="input-field">
                  <?php echo form_label('<span class="red-text">*</span> '.lang('create_user_fname_label'), 'first_name');?>
                  <?php echo form_input($first_name);?>
                  <?php echo form_error('first_name','<div class="red-text">','</div>'); ?>
            </p>

            <p class="input-field">
                  <?php echo form_label('<span class="red-text">*</span> '.lang('create_user_lname_label'), 'last_name');?>
                  <?php echo form_input($last_name);?>
                  <?php echo form_error('last_name','<div class="red-text">','</div>'); ?>
            </p>

            <?php
            if($identity_column!=='email') {
                echo '<p class="input-field">';
                echo form_label('<span class="red-text">*</span> '.lang('create_user_identity_label'), 'identity');
                echo '<br />';
                echo form_input($identity);
                echo form_error('identity','<div class="red-text">','</div>');
                echo '</p>';
            }
            ?>

            <p class="input-field">
                  <?php echo form_label('<span class="red-text">*</span> '.lang('create_user_email_label'), 'email');?>
                  <?php echo form_input($email);?>
                  <?php echo form_error('email','<div class="red-text">','</div>'); ?>
            </p>

            <p class="input-field" style="display:none">
                  <?php echo form_label('<span class="red-text">*</span> '.lang('create_user_phone_label'), 'phone');?>
                  <?php echo form_input($phone);?>
                  <?php echo form_error('phone','<div class="red-text">','</div>'); ?>
            </p>

            <p class="input-field">
            	  <?php echo form_label('<span class="red-text">*</span> '.lang('create_user_password_label'), 'password');?>
                  <?php echo form_input($password);?>
                  <?php echo form_error('password','<div class="red-text">','</div>'); ?>
            </p>

            <p class="input-field">
            	  <?php echo form_label('<span class="red-text">*</span> '.lang('create_user_password_confirm_label'), 'password_confirm');?>
                  <?php echo form_input($password_confirm);?>
                  <?php echo form_error('password_confirm','<div class="red-text">','</div>'); ?>
            </p>

            <p>
              <label>
                <input type="checkbox" name="terms" value="1" />
                <span>Prihvatam <a href="/uslovi-koriscenja">Prihvatam uslove korišćenja</a></span>
                <?php echo form_error('terms','<div class="red-text">','</div>'); ?>
              </label>
            </p>

            <p>
              <button name="submit" type="submit" class="btn  white-text waves-effect waves-light"><i class="fa fa-check left"></i>Nastavi</button>
              <a href="/auth" class="btn  white-text waves-effect waves-light"><i class="fa fa-times  left"></i>Odustani</a>
            </p>

      <?php echo form_close();?>
    </div>
  </div>
</div>
</div>
