<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <div class="container">
    <div class="section">
      <div class="row">
        <div class="col s12">
          <h1 class="center-align">Pronađi muziku</h1>
          <p class="center-align">Za vas smo izdvojili najbolje bendove za svadbe sa Balkana. Ako vam treba muzika i bend za venčanje potražite u našoj bazi muziku po svom ukusu.</p>
          <form id="Search" class="center-align">
            <input  id="searchQuery" class="center-align" placeholder="Naziv grupe, grad">
            <button class="btn waves-effect waves-light searchBtn" type="submit" name="action">PRETRAŽI
              <i class="material-icons right">search</i>
            </button>
          </form>
        </div>

        <div id="findMusicSidebar" class="col s12 m3">
          <div class="row">
            <div class="col s12 center-align">
              <button class="btn waves-effect waves-light resetBtn" type="submit" name="action">Poništi filter
                <i class="material-icons right">close</i>
              </button>
            </div>
            <div class="col s12">
              <p class="title">Datum</p>
              <p class="subtitle">Unesite datum proslave</p>
            </div>
            <div class="col s12 m6">
              <input type="text" placeholder="Datum od" class="datepicker tooltipped" data-position="top" data-tooltip="Ukoliko želite da angažujete bend na jedan dan odaberite samo datum od ">
            </div>
            <div class="col s12 m6">
              <input type="text" placeholder="Datum do" class="datepicker tooltipped" data-position="top" data-tooltip="Ukoliko želite da angažujete bend na više dana odaberite i datum do ">
            </div>
            <div class="col s12">
              <p class="title">Cena</p>
              <input type="text" class="js-range-slider" name="my_range" value="" />
              <input type="hidden" name="priceFrom" id="priceFrom">
              <input type="hidden" name="priceTo" id="priceTo">
            </div>

            <div class="col s12">
              <div class="divider"></div>
              <p class="title">Tip grupe</p>
              <p class="subtitle">Izaberite tip grupe</p>
              <?php foreach ($bands_type_list as $bands_type) { ?>
                <label>
                  <input type="checkbox" id="bands_type_id_<?php echo $bands_type->id ?>" name="bands_type" value="<?php echo $bands_type->id ?>"/>
                  <span> <?php echo $bands_type->band_type_name ?></span>
                </label>
                <br>
              <?php } ?>
            </div>

            <div class="col s12">
              <div class="divider"></div>
              <p class="title">Žanr muzike</p>
              <p class="subtitle">Izaberite žanr muzike</p>
              <?php foreach ($genres_list as $genre) { ?>
                <label>
                  <input type="checkbox" id="genre_id_<?php echo $genre->id ?>" name="genre" value="<?php echo $genre->id ?>"/>
                  <span> <?php echo $genre->genre_name ?> </span>
                </label>
                <br>
              <?php } ?>
            </div>

            <div class="col s12">
              <div class="divider"></div>

              <p class="title">Tip proslave</p>
                <p class="subtitle">Izaberite tip proslave</p>
                <?php foreach ($parties_list as $party) { ?>
                  <label>
                    <input type="checkbox" id="party_id_<?php echo $party->id ?>" name="party" value="<?php echo $party->id ?>"/>
                    <span> <?php echo $party->party_name ?></span>
                  </label>
                  <br>
                <?php } ?>
            </div>
          </div>
        </div> <!--end of filterForm -->
        <div id="findMusicContent" class="col s12 m9">
          <div class="row">
            <div class="col s12">
              <h5>Broj rezultata: <span id="resultCounter">12</span></h5>
            </div>
            <div class="col s12">
              <div class="divider"></div>
                <div class="row">
                  <div class="input-field col s12">
                    <select name="sortBy">
                      <option value="1" selected>Po ceni od najjeftinijeg</option>
                      <option value="2">Po ceni od najskupljeg</option>
                      <option value="3">Najnoviji</option>
                      <option value="4">Najstariji</option>
                  </select>
                </div>
                <div id="filterResults" class="col s12">
                  <div class="card">
                    <div class="card-image waves-effect waves-block waves-light col s6">
                      <img class="activator" src="/files/profile/profile_template.jpg">
                    </div>
                    <div class="card-content col s6">
                      <span class="card-title activator grey-text text-darken-4">Card Title<i class="material-icons right">more_vert</i></span>
                      <p><a href="#">This is a link</a></p>
                    </div>
                    <div class="card-reveal">
                      <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
                      <p>Here is some more information about this product that is only revealed once clicked on.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><!--end of filterResults -->
    </div>
  </div>


<script>
  $(".js-range-slider").ionRangeSlider({
      type: "double",
      step: 50,
      min: 0,
      max: 5000,
      from: 0,
      to: 5000,
      grid: true,
      onStart: function (data) {
        priceFrom = data.from;
        priceTo = data.to;
      },
      onFinish: function (data) {
         $('#priceFrom').val(data.from);
         $('#priceTo').val(data.to);
       },
  });
</script>
