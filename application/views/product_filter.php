<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <div class="container">
    <div class="section">
      <div class="row">
        <div class="col s12">
          <h1 class="center-align">Pronađi muziku | Zvezde Granda</h1>
          <p class="center-align">Za vas smo izdvojili najbolje bendove za svadbe sa Balkana. Ako vam treba muzika i bend za venčanje potražite u našoj bazi muziku po svom ukusu.</p>
          <div id="Search" class="center-align">
            <input  id="searchQuery" class="center-align" placeholder="Naziv grupe, grad">
            <button id="searchBtn" class="btn waves-effect waves-light">PRETRAŽI
              <i class="material-icons right">search</i>
            </button>
          </div>
        </div>

        <div id="findMusicSidebar" class="col s12 m3">
          <div class="row">
            <div class="col s12 center-align">
              <button id="resetBtn" class="btn waves-effect waves-light" type="submit" name="action">Poništi filter
                <i class="material-icons right">close</i>
              </button>
            </div>
            <div class="col s12">
              <p class="title">Datum</p>
              <p class="subtitle">Unesite datum proslave</p>
            </div>
            <div class="col s6 m6">
              <input type="text" name="dateFrom" id="dateFrom" placeholder="Datum od" class="datepicker_filter tooltipped" data-position="top" data-tooltip="Ukoliko želite da angažujete bend na jedan dan odaberite samo datum od ">
            </div>
            <div class="col s6 m6">
              <input type="text" name="dateTo" id="dateTo" placeholder="Datum do" class="datepicker_filter tooltipped" data-position="top" data-tooltip="Ukoliko želite da angažujete bend na više dana odaberite i datum do ">
            </div>
            <div class="col s12">
              <p class="title">Cena</p>
              <input type="text" class="js-range-slider" name="my_range" value="" />
              <input type="hidden" name="priceFrom" value="50" id="priceFrom"/>
              <input type="hidden" name="priceTo" value="5000" id="priceTo"/>
            </div>

            <div class="col s12">
              <div class="divider"></div>
              <p class="title">Tip grupe</p>
              <p class="subtitle">Izaberite tip grupe</p>
              <?php foreach ($bands_type_list as $bands_type) { ?>
                <label>
                  <input class="common_selector band_type" type="checkbox" id="bands_type_id_<?php echo $bands_type->id ?>" name="bands_type" value="<?php echo $bands_type->id ?>"/>
                  <span> <?php echo $bands_type->band_type_name ?></span>
                </label>
                <br>
              <?php } ?>
            </div>

            <div class="col s12">
              <div class="divider"></div>
              <p class="title">Žanr muzike</p>
              <p class="subtitle">Izaberite žanr muzike</p>
              <?php foreach ($genres_list as $genre) { ?>
                <label>
                  <input class="common_selector genre" type="checkbox" id="genre_id_<?php echo $genre->id ?>" name="genre" value="<?php echo $genre->id ?>"/>
                  <span> <?php echo $genre->genre_name ?> </span>
                </label>
                <br>
              <?php } ?>
            </div>

            <div class="col s12">
              <div class="divider"></div>

              <p class="title">Tip proslave</p>
                <p class="subtitle">Izaberite tip proslave</p>
                <?php foreach ($parties_list as $party) { ?>
                  <label>
                    <input class="common_selector partie" type="checkbox" id="party_id_<?php echo $party->id ?>" name="party" value="<?php echo $party->id ?>"/>
                    <span> <?php echo $party->party_name ?></span>
                  </label>
                  <br>
                <?php } ?>
            </div>
          </div>
        </div> <!--end of filterForm -->
        <div id="findMusicContent" class="col s12 m9">
          <div class="row">
            <div class="col s12">
              <h5>Broj rezultata: <span id="resultCounter">12</span></h5>
            </div>
            <div class="col s12">
              <div class="divider"></div>
                <div class="row">
                  <div class="input-field col s12">
                    <select name="sortBy">
                      <option value="1" selected>Po ceni od najjeftinijeg</option>
                      <option value="2">Po ceni od najskupljeg</option>
                      <option value="3">Najnoviji</option>
                      <option value="4">Najstariji</option>
                  </select>
                </div>
                <div id="filterResults" class="col s12">
                  <div align="center" id="pagination_link">

                </div>
                  <div class="row filter_data">

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><!--end of filterResults -->
    </div>
  </div>


<script>
  $(document).ready(function(){

    $('.datepicker_filter').datepicker({
      format: 'dd.mmmm.yyyy',
      minDate: new Date(),
      firstDay: 1,
      i18n: {
              cancel: 'Odustani',
              clear: 'Poništi',
              done: 'Prihvati',
              months: ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"],
              monthsShort: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Avg", "Sep", "Okt", "Nov", "Dec"],
              weekdays: ["Nedelja","Ponedeljak", "Utorak", "Sreda", "Četvrtak", "Petak", "Subota"],
              weekdaysShort: ["Ned","Pon", "Uto", "Sre", "Čet", "Pet", "Sub"],
              weekdaysAbbrev:	['N','P','U','S','Č','P','S']
            },
      onClose: function(){
        var id = this.id;
  						alert(id); 
      },
    });


    filter_data(1);

    function filter_data(page)
    {
        $('.filter_data').html('<div id="loading" style="" ></div>');
        var action = 'fetch_data';
        var minimum_price = $('#priceFrom').val();
        var maximum_price = $('#priceTo').val();
        var band = get_filter('band_type');
        var genre = get_filter('genre');
        var partie = get_filter('partie');
				var searchquery = $('#searchQuery').val();
        console.log(searchquery);
        $.ajax({
            url:"<?php echo base_url(); ?>product_filter/fetch_data/"+page,
            method:"POST",
            dataType:"JSON",
            data:{action:action, minimum_price:minimum_price, maximum_price:maximum_price, band:band, genre:genre, partie:partie, searchquery:searchquery},
            success:function(data)
            {
                $('.filter_data').html(data.product_list);
                $('#pagination_link').html(data.pagination_link);
                console.log(data.pagination_link);
            }
        })
    }

    function get_filter(class_name)
    {
        var filter = [];
        $('.'+class_name+':checked').each(function(){
            filter.push($(this).val());
        });
        console.log( filter);
        return filter;

    }

    $(document).on('click', '.pagination li a', function(event){
        event.preventDefault();
        var page = $(this).data('ci-pagination-page');
        filter_data(page);
    });

    $('.common_selector').click(function(){
        filter_data(1);
    });

		$("#searchBtn").click(function(){
        filter_data(1);
    });

    $("#resetBtn").click(function(){
      $( ".common_selector" ).prop( "checked", false );
        let my_range = $(".js-range-slider").data("ionRangeSlider");
        my_range.reset();
        filter_data(1);
    });


    $(".js-range-slider").ionRangeSlider({
      type: "double",
      step: 50,
      min: 50,
      max: 5000,
      from: 50,
      to: 5000,
      grid: true,
      onStart: function (data) {
        priceFrom = data.from;
        priceTo = data.to;
        //console.log(data.from,data.to );
      },
      onFinish: function (data) {
         $('#priceFrom').val(data.from);
         $('#priceTo').val(data.to);
         //console.log(data.from,data.to );
         filter_data(1);
       },
     });
  });
</script>
