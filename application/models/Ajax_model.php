<?php
class Ajax_model extends CI_Model {
  public function create_header_background($data) {
    $this->db->select('band_cover_image');
    $this->db->where('user_id',  $data['user_id']);
    $query = $this->db->get('bands');
    if ($query->num_rows() > 0) {
        //record exists - do update
        $this->db->set('band_cover_image', $data['band_cover_image']);
        $this->db->where('user_id', $data['user_id']);
        $this->db->update('bands');
      }
      else
      {
        if ($this->db->insert('bands', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
      }
  }

  public function change_band_name($data) {
    $this->db->select('band_name');
    $this->db->where('user_id',  $data['user_id']);
    $query = $this->db->get('bands');
    if ($query->num_rows() > 0) {
        //record exists - do update
        $this->db->set('band_name', $data['band_name']);
        $this->db->set('band_url', $data['band_url']);
        $this->db->where('user_id', $data['user_id']);
        $this->db->update('bands');
      }
      else
      {
        if ($this->db->insert('bands', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
      }
  }

  public function create_profile_image($data) {
    $this->db->select('band_profile_image');
    $this->db->where('user_id', $data['user_id']);
    $query = $this->db->get('bands');
    if ($query->num_rows() > 0) {
        $this->db->set('band_profile_image', $data['band_profile_image']);
        $this->db->where('user_id', $data['user_id']);
        $this->db->update('bands');
      }
      else
      {
        if ($this->db->insert('bands', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
      }
  }

  public function create_personal_information($data) {
    $this->db->set('city', $data['city']);
    $this->db->set('phone', $data['phone']);
    $this->db->set('email', $data['email']);
    $this->db->set('price_from', $data['price_from']);
    $this->db->set('price_to', $data['price_to']);
    $this->db->where('user_id', $data['user_id']);
    $this->db->update('bands');
  }

  function get_band_id($userID)
    {
      $this->db->select('id');
      $this->db->where('user_id', $userID);
      $query = $this->db->get('bands');
      return $query->row();
    }

  public function delete_parties($band_id) {
    $this->db->where('band_id', $band_id);
    $this->db->delete('bands_has_parties');
  }

  public function create_parties($data) {
    $this->db->insert('bands_has_parties', $data);
  }

  public function delete_genres($band_id) {
    $this->db->where('band_id', $band_id);
    $this->db->delete('bands_has_genres');
  }

  public function create_genres($data) {
    $this->db->insert('bands_has_genres', $data);
  }

  public function delete_band_type($band_id) {
    $this->db->where('band_id', $band_id);
    $this->db->delete('bands_has_types');
  }

  public function create_band_type($data) {
    $this->db->set('type_id', $data['type_id']);
    $this->db->set('user_id', $data['user_id']);
    $this->db->set('band_id', $data['band_id']);
    $this->db->insert('bands_has_types', $data);
  }

  public function delete_instruments($band_id) {
    $this->db->where('band_id', $band_id);
    $this->db->delete('bands_has_instruments');
  }

  public function create_instruments($data) {
    $this->db->insert('bands_has_instruments', $data);
  }

  public function create_description($data) {
    $this->db->set('description', $data['description']);
    $this->db->where('id', $data['band_id']);
    $this->db->update('bands');
  }

  public function create_photos($data) {
    //$this->db->insert('bands_has_photos', $data);
    if ($this->db->insert('bands_has_photos', $data)) {
      return $this->db->insert_id();
    } else {
      return false;
    }
  }

  public function delete_photo($photo_id) {
    $this->db->where('id', $photo_id);
    $this->db->delete('bands_has_photos');
  }

  public function delete_youtube($user_id) {
    $this->db->where('user_id', $user_id);
    $this->db->delete('bands_has_youtubes');
  }

  public function create_youtube($data) {
    if ($this->db->insert('bands_has_youtubes', $data)) {
      return $this->db->insert_id();
    } else {
      return false;
    }
  }


  public function delete_youtube_link($youtube_id) {
    $this->db->where('id', $youtube_id);
    $this->db->delete('bands_has_youtubes');
  }

}
