<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Band_model extends CI_Model{
function __construct() {
parent::__construct();
}

function get_band_id_by_url($band_url)
  {
    $this->db->select('id');
    $this->db->where('band_url', $band_url);
    $query = $this->db->get('bands');
    return $query->row();

  }

  function get_band_id_by_user_id($user_id)
    {
      $this->db->select('id');
      $this->db->where('user_id', $user_id);
      $query = $this->db->get('bands');
      return $query->row();
    }

function get_profile_type($userID)
  {
    $this->db->select('profile.profile_type');
    $this->db->join('profile', 'profile.id = users_has_profiles.profile_id',  'inner');
    $this->db->where('users_has_profiles.user_id', $userID);
    $query = $this->db->get('users_has_profiles');
    return $query->row();
  }

function get_profile_url($userID)
  {
    $this->db->select('profile.profile_url');
    $this->db->join('profile', 'profile.id = users_has_profiles.profile_id',  'inner');
    $this->db->where('users_has_profiles.user_id', $userID);
    $query = $this->db->get('users_has_profiles');
    return $query->row();
  }

function get_profile_list()
  {
    $this->db->select('*');
    $this->db->order_by('profile_order', 'ASC');
    $query = $this->db->get('profile');
    return $query->result();
  }

function get_parties_list()
  {
    $this->db->select('*');
    $this->db->order_by('party_order', 'ASC');
    $query = $this->db->get('parties');
    return $query->result();
  }

function get_genres_list()
  {
    $this->db->select('*');
    $this->db->order_by('genre_order', 'ASC');
    $query = $this->db->get('genres');
    return $query->result();
  }

function get_bands_type_list()
  {
    $this->db->select('*');
    $this->db->order_by('band_type_order', 'ASC');
    $query = $this->db->get('bands_type');
    return $query->result();
  }

  function get_instruments_list()
    {
      $this->db->select('*');
      $this->db->order_by('instrument_order', 'ASC');
      $query = $this->db->get('instruments');
      return $query->result();
    }

function crate_user_profile_type($data)
  {
    $this->db->insert('users_has_profiles', $data);
  }

function get_band_details($bandID)
  {
    $this->db->select('*');
    //$this->db->join('profile', 'profile.id = users_has_profiles.profile_id',  'inner');
    $this->db->where('id', $bandID);
    $query = $this->db->get('bands');
    return $query->row();
  }

function get_band_have_parties($bandID)
  {
    $this->db->select('parties.id, parties.party_name');
    $this->db->join('parties', 'parties.id = bands_has_parties.party_id',  'inner');
    $this->db->where('bands_has_parties.band_id', $bandID);
    $this->db->order_by('parties.party_order', 'ASC');
    $query = $this->db->get('bands_has_parties');
    return $query->result();
  }

function get_band_have_genres($bandID)
  {
    $this->db->select('genres.id, genres.genre_name');
    $this->db->join('genres', 'genres.id = bands_has_genres.genre_id',  'inner');
    $this->db->where('bands_has_genres.band_id', $bandID);
    $this->db->order_by('genres.genre_order', 'ASC');
    $query = $this->db->get('bands_has_genres');
    return $query->result();
  }

function get_band_have_types($bandID)
  {
    $this->db->select('bands_type.id, bands_type.band_type_name');
    $this->db->join('bands_type', 'bands_type.id = bands_has_types.type_id',  'inner');
    $this->db->where('bands_has_types.band_id', $bandID);
    $this->db->order_by('bands_type.band_type_order', 'ASC');
    $query = $this->db->get('bands_has_types');
    return $query->result();
  }

function get_band_have_instruments($bandID)
  {
    $this->db->select('instruments.id, instruments.instrument_name');
    $this->db->join('instruments', 'instruments.id = bands_has_instruments.instrument_id',  'inner');
    $this->db->where('bands_has_instruments.band_id', $bandID);
    $this->db->order_by('instruments.instrument_order', 'ASC');
    $query = $this->db->get('bands_has_instruments');
    return $query->result();
  }

  function get_band_have_photos($bandID)
    {
      $this->db->select('*');
      $this->db->where('band_id', $bandID);
      $query = $this->db->get('bands_has_photos');
      return $query->result();
    }

  function get_band_have_youtubes($bandID)
    {
      $this->db->select('*');
      $this->db->where('band_id', $bandID);
      $this->db->order_by('id', 'ASC');
      $query = $this->db->get('bands_has_youtubes');
      return $query->result();
    }

    function get_band_have_events($bandID)
      {
        $this->db->select('*');
        $this->db->where('band_id', $bandID);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('bands_has_events');
        return $query->result();
      }

}
?>
