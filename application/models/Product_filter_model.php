<?php

class Product_filter_model extends CI_Model
{
 function fetch_filter_type($type)
 {
  $this->db->distinct();
  $this->db->select($type);
  $this->db->from('product');
  $this->db->where('product_status', '1');
  return $this->db->get();
 }

 function make_query($minimum_price, $maximum_price, $band_type, $genre, $partie, $searchquery)
 {
  $query = "
  SELECT * FROM bands
  ";

  if(isset($band_type))
  {
   $band_filter = implode("','", $band_type);
   $query .= "
   INNER JOIN bands_has_types
   ON bands.id=bands_has_types.band_id
   AND bands_has_types.type_id IN('".$band_filter."')
   ";
  }

  if(isset($genre))
  {
   $genre_filter = implode("','", $genre);
   $query .= "
   INNER JOIN bands_has_genres
   ON bands.id=bands_has_genres.band_id
   AND bands_has_genres.genre_id IN('".$genre_filter."')
   ";
  }

  if(isset($partie))
  {
   $party_filter = implode("','", $partie);
   $query .= "
   INNER JOIN bands_has_parties
   ON bands.id=bands_has_parties.band_id
   AND bands_has_parties.party_id IN('".$party_filter."')
   ";
  }

  $query .= "
  WHERE active = '1'
  ";

  //$string = "galax";
  if(isset($searchquery))
  {
   $string_filter = $searchquery;
   $query .= "
    AND (band_name LIKE('%".$string_filter."%') OR city LIKE('%".$string_filter."%'))
   ";
  }

  if(isset($minimum_price, $maximum_price) && !empty($minimum_price) &&  !empty($maximum_price))
{
 $query .= "
  AND ((price_from BETWEEN '".$minimum_price."' AND '".$maximum_price."') OR (price_from <= '".$maximum_price."'))
 ";
 // $query .= "
 //  AND price_to >= '".$maximum_price."'
 // ";
}


  return $query;
 }

 function count_all($minimum_price, $maximum_price, $band_type, $genre, $partie, $searchquery)
 {
  $query = $this->make_query($minimum_price, $maximum_price, $band_type, $genre, $partie, $searchquery);
  $data = $this->db->query($query);
  return $data->num_rows();
 }

 function fetch_data($limit, $start, $minimum_price, $maximum_price, $band_type, $genre, $partie, $searchquery)
 {
  $query = $this->make_query($minimum_price, $maximum_price, $band_type, $genre, $partie, $searchquery);

  $query .= ' LIMIT '.$start.', ' . $limit;

  $data = $this->db->query($query);

  $output = '';
  if($data->num_rows() > 0)
  {
   foreach($data->result_array() as $row)
   {
    $output .= '
    <div class="col s4 m6">
     <div style="border:1px solid #ccc; border-radius:5px; padding:16px; margin-bottom:16px; height:450px;">
      <img src="'.base_url().'files/profile/'. $row['band_profile_image'] .'" alt="" class="img-responsive" >
      <p align="center"><strong><a href="#">'. $row['band_name'] .'</a></strong></p>
      <h4 style="text-align:center;" class="text-danger" >'. $row['city'] .'</h4>
     </div>
    </div>
    ';
   }
  }
  else
  {
   $output = '<h3>No Data Found</h3>';
  }
  return $output;
 }
}

?>
