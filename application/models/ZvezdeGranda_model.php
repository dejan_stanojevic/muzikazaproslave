<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class zvezdeGranda_model extends CI_Model{
function __construct() {
parent::__construct();
}

function get_zvezde_granda()
  {
    $this->db->select('bands.band_name, bands.band_profile_image, bands.band_url, bands.band_name, bands.description, bands.city');
    $this->db->join('bands', 'bands.id = zvezde_granda.band_id',  'inner');
    $query = $this->db->get('zvezde_granda');
    return $query->result();
  }

function get_profile_type($userID)
  {
    $this->db->select('profile.profile_type');
    $this->db->join('profile', 'profile.id = users_has_profiles.profile_id',  'inner');
    $this->db->where('users_has_profiles.user_id', $userID);
    $query = $this->db->get('users_has_profiles');
    return $query->row();
  }

  function get_band_id($userID)
    {
      $this->db->select('id');
      $this->db->where('user_id', $userID);
      $query = $this->db->get('bands');
      return $query->row();
    }

function get_profile_url($userID)
  {
    $this->db->select('profile.profile_url');
    $this->db->join('profile', 'profile.id = users_has_profiles.profile_id',  'inner');
    $this->db->where('users_has_profiles.user_id', $userID);
    $query = $this->db->get('users_has_profiles');
    return $query->row();
  }

function get_profile_list()
  {
    $this->db->select('*');
    $this->db->order_by('profile_order', 'ASC');
    $query = $this->db->get('profile');
    return $query->result();
  }

function get_parties_list()
  {
    $this->db->select('*');
    $this->db->order_by('party_order', 'ASC');
    $query = $this->db->get('parties');
    return $query->result();
  }

function get_genres_list()
  {
    $this->db->select('*');
    $this->db->order_by('genre_order', 'ASC');
    $query = $this->db->get('genres');
    return $query->result();
  }

function get_bands_type_list()
  {
    $this->db->select('*');
    $this->db->order_by('band_type_order', 'ASC');
    $query = $this->db->get('bands_type');
    return $query->result();
  }

  function get_instruments_list()
    {
      $this->db->select('*');
      $this->db->order_by('instrument_order', 'ASC');
      $query = $this->db->get('instruments');
      return $query->result();
    }

function crate_user_profile_type($data)
  {
    $this->db->insert('users_has_profiles', $data);
  }

function get_band_details($userID)
  {
    $this->db->select('*');
    //$this->db->join('profile', 'profile.id = users_has_profiles.profile_id',  'inner');
    $this->db->where('bands.user_id', $userID);
    $query = $this->db->get('bands');
    return $query->row();
  }

function get_band_have_parties($userID)
  {
    $this->db->select('parties.id, parties.party_name');
    $this->db->join('parties', 'parties.id = bands_has_parties.party_id',  'inner');
    $this->db->where('bands_has_parties.user_id', $userID);
    $this->db->order_by('parties.party_order', 'ASC');
    $query = $this->db->get('bands_has_parties');
    return $query->result();
  }

function get_band_have_genres($userID)
  {
    $this->db->select('genres.id, genres.genre_name');
    $this->db->join('genres', 'genres.id = bands_has_genres.genre_id',  'inner');
    $this->db->where('bands_has_genres.user_id', $userID);
    $this->db->order_by('genres.genre_order', 'ASC');
    $query = $this->db->get('bands_has_genres');
    return $query->result();
  }

function get_band_have_types($userID)
  {
    $this->db->select('bands_type.id, bands_type.band_type_name');
    $this->db->join('bands_type', 'bands_type.id = bands_has_types.type_id',  'inner');
    $this->db->where('bands_has_types.user_id', $userID);
    $this->db->order_by('bands_type.band_type_order', 'ASC');
    $query = $this->db->get('bands_has_types');
    return $query->result();
  }

function get_band_have_instruments($userID)
  {
    $this->db->select('instruments.id, instruments.instrument_name');
    $this->db->join('instruments', 'instruments.id = bands_has_instruments.instrument_id',  'inner');
    $this->db->where('bands_has_instruments.user_id', $userID);
    $this->db->order_by('instruments.instrument_order', 'ASC');
    $query = $this->db->get('bands_has_instruments');
    return $query->result();
  }

  function get_band_have_photos($userID)
    {
      $this->db->select('*');
      $this->db->where('user_id', $userID);
      $query = $this->db->get('bands_has_photos');
      return $query->result();
    }

    function get_band_have_youtubes($userID)
      {
        $this->db->select('*');
        $this->db->where('user_id', $userID);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('bands_has_youtubes');
        return $query->result();
      }

function get_courses_list()
  {
    $this->db->select('courses.id, courses.course_code, courses.course_name, courses_has_dates.course_id, courses_has_dates.course_date');
    $this->db->join('courses_has_dates', 'courses_has_dates.course_id = courses.id',  'inner');
    $query = $this->db->get('courses');
    return $query->result();
  }

function get_courses_date()
  {
    $this->db->select('course_id,course_date');
    $query = $this->db->get('courses_has_dates');
    return $query->result();
  }

function courses_date_by_id($courseID)
  {
    $this->db->select('course_date, courses.course_count');
    $this->db->join('courses', 'courses.id = courses_has_dates.course_id','inner');
    $this->db->where('course_id',  $courseID);
    $query = $this->db->get('courses_has_dates');
    return $query->result();
    //return $query->row();
  }

function get_child_courses_by_id($courseID)
  {
    $this->db->select('course_child');
    $this->db->where('course_parent',  $courseID);
    $query = $this->db->get('courses_has_packages');
    return $query->result();
  }

function course_set_count($course_id, $course_counter)
  {
    $this->db->set('course_count', $course_counter, FALSE);
    $this->db->where('id', $course_id);
    $this->db->update('courses');
  }
}
?>
