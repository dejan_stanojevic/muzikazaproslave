<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Welcome_model extends CI_Model{
function __construct() {
parent::__construct();
}

function get_recomended_list()
  {
    $this->db->select('*');
    $this->db->join('bands', 'bands.id = recomended_bands.band_id',  'inner');
    $this->db->order_by('band_order', 'ASC');
    $query = $this->db->get('recomended_bands');
    return $query->result();

  }

function get_parties_list()
  {
    $this->db->select('*');
    $this->db->order_by('party_order', 'ASC');
    $query = $this->db->get('parties');
    return $query->result();
  }

function get_genres_list()
  {
    $this->db->select('*');
    $this->db->order_by('genre_order', 'ASC');
    $query = $this->db->get('genres');
    return $query->result();
  }

function get_bands_type_list()
  {
    $this->db->select('*');
    $this->db->order_by('band_type_order', 'ASC');
    $query = $this->db->get('bands_type');
    return $query->result();
  }

function get_instruments_list()
  {
    $this->db->select('*');
    $this->db->order_by('instrument_order', 'ASC');
    $query = $this->db->get('instruments');
    return $query->result();
  }
}
?>
