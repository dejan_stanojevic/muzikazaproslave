<?php

class Fullcalendar_model extends CI_Model
{
	function fetch_all_event($band_id){
		$this->db->order_by('id');
		if (isset($band_id)) {
		$this->db->where('band_id', $band_id);
		}
		return $this->db->get('bands_has_events');
	}

	function insert_event($data)
	{
		$this->db->insert('bands_has_events', $data);
	}

	function update_event($data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update('bands_has_events', $data);
	}

	function delete_event($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('bands_has_events');
	}
}

?>
