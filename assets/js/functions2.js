$(document).ready(function() {
  $('.sidenav').sidenav();
	$('.dropdown-trigger').dropdown({
		 inDuration: 300,
		 outDuration: 225,
		 constrainWidth: true, // Does not change width of dropdown to that of the activator
     coverTrigger: false,
		 hover: true, // Activate on hover
		 gutter: 0, // Spacing from edge
		 belowOrigin: true, // Displays dropdown below the button
		 alignment: 'right', // Displays dropdown with edge aligned to the left of button
		 stopPropagation: false
		}
	);
  $('.modal').modal();

  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    navText: ["<i class='material-icons'>chevron_left</i>", "<i class='material-icons'>chevron_right</i>"],
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:5,
            nav:true,
            loop:false
        }
    }
});

$('.datepicker').datepicker({
  format: 'dd.mmmm.yyyy',
  minDate: new Date(),
  firstDay: 1,
  i18n: {
          cancel: 'Odustani',
          clear: 'Poništi',
          done: 'Prihvati',
          months: ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"],
          monthsShort: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Avg", "Sep", "Okt", "Nov", "Dec"],
          weekdays: ["Nedelja","Ponedeljak", "Utorak", "Sreda", "Četvrtak", "Petak", "Subota"],
          weekdaysShort: ["Ned","Pon", "Uto", "Sre", "Čet", "Pet", "Sub"],
          weekdaysAbbrev:	['N','P','U','S','Č','P','S']
        },
});

  $('.login-form').on('submit', function(e) {
    e.preventDefault();
    var identity = $("#identity").val();
    var password = $("#password").val();
        $.ajax({
            url: "/auth/ajax_login",
            type: "post",
            data: {identity: identity, password: password},
            cache: false,
            success: function (json) {
               console.log (json);
                var error_message = json.error;
                var success = json.logged_in;
                var message = json.message;
                if (typeof error_message !== "undefined") {
                    $("#modal .message").html(error_message);
                    $('#modal ').modal('open');
                }
                else if (typeof success !== "undefined" && success == "1") {
                  $("#modal .message").html(message);
                  $("#modal .modal-icon").html('<i class="large material-icons green-text">check_circle_outline</i>');
                    $('#modal').modal({
                        //onCloseStart: function() { window.location.href = "/" }
                        onCloseEnd: function() { window.location.href = "/profile" }
                      }
                    );
                    $('#modal').modal('open');
                }
                else {
                  $("#modal .message").html(message);
                  $("#modal .modal-icon").html('<i class="large material-icons red-text">error_outline</i>');
                  $('#modal ').modal('open');
                    // $(".username_error").html(json.username_error);
                    // $(".password_error").html(json.password_error);
                }
            }
        });
  });

  $('select').formSelect();

  $('.tooltipped').tooltip();


  //create $profile_type

  $(".create_user_profile_type").click(function (e) {
    e.preventDefault();
    var profile_type_id = $(this).data('profile');
    var profile_type_name = $(this).data('profiletype');
    //console.log(profile_type_id);
    $.ajax({
        url: "/profile/crate_user_profile_type",
        type: "post",
        data: {profile_type_id: profile_type_id, profile_type_name: profile_type_name},
        cache: false,
        success: function (json) {
           console.log (json);
           var message = json.message;
           $("#modalProfile .message").html(message);
           $("#modalProfile h4").html(profile_type_name);
           $("#modalProfile .modal-icon").html('<i class="large material-icons green-text">check_circle_outline</i>');
           $('#modalProfile').modal({
                 //onCloseStart: function() { window.location.href = "/" }
                 onCloseEnd: function() { window.location.href = "/profile" }
               }
             );
            $('#modalProfile').modal('open');

        }
    });
  });

  //upload profile Background
  $('a#profileBackground').click(function(e){
    e.preventDefault();
    $('input#uploadHeaderBackground').click();
  });

  $('input#uploadHeaderBackground').change(function(){
    var inputFile = $('input[name=headerBackground]');
    var progressBar = $('.determinate');
    var fileToUpload = inputFile[0].files[0];
    var formData = new FormData();
    if (fileToUpload == null) {
      alert("izaberite sliku");

    } else {
      var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
      if(!allowedExtensions.exec(fileToUpload.name)){
        alert('Dozvoljeni formati su .jpeg/.jpg/.png/.gif.');
      } else {
      // provide the form data
        var maxFileSize = 8;
        var fileSize = (fileToUpload.size);
        if (fileSize > maxFileSize * 1024 * 1024){
        alert(fileToUpload.name + " is " + Math.floor(fileToUpload.size/1024/1024)+ " Mbytes which exceeds the maximum allowed file upload size of " + maxFileSize + " Mbytes.");
      } else {
        progressBar.css({width:  "0%"});
        formData.append("userfile", fileToUpload);
        // now upload the file using $.ajax
        $.ajax({
            url: '/ajax/uploadheader',
            type: 'post',
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
              var obj = JSON.parse(data)
              var band_cover_image = "/files/header/" + obj.band_cover_image;
              $('#headerProfile').css('background-image','url(' + band_cover_image + ')');
              //$('#headerProfile').css('display', 'inline-block');
              // $('.licence-upload .removeFile').css('display', 'inline-block');
              // $('#driver_licence_file').val(obj.image);
            },
            xhr: function () {
                var xhr = new XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (event) {
                    if (event.lengthComputable) {
                        var percentComplete = Math.round((event.loaded / event.total) * 100);
                        // console.log(percentComplete);
                        $('.progress').show();
                        progressBar.css({width: percentComplete + "%"});
                        $("#upload-btn").attr("disabled", true);
                        progressBar.text(percentComplete + '%');
                    }
                    ;
                }, false);
                return xhr;
            }
        });
    }
  }
}
});



$( "#profileName" ).blur(function() {
  var newName = $(this).text();
  $.ajax({
      url: '/ajax/change_band_name',
      type: 'post',
      data: {band_name: newName},
      success: function (response) {
        console.log(response);
        $("#modal .message").html("Naziv grupe je uspešno promenjen");
        $("#modal .modal-icon").html('<i class="large material-icons green-text">check_circle_outline</i>');
        $('#modal').modal('open');
      }
  });

});

//upload header profile image

$('a#profileImage').click(function(e){
  e.preventDefault();
  $('input#uploadProfileImage').click();
});

$('input#uploadProfileImage').change(function(){
  var inputFile = $('input[name=profileImage]');
  var progressBar = $('.determinate');
  var fileToUpload = inputFile[0].files[0];
  var formData = new FormData();
  if (fileToUpload == null) {
    alert("Izaberite sliku");

  } else {
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if(!allowedExtensions.exec(fileToUpload.name)){
      alert('Dozvoljeni formati su .jpeg/.jpg/.png/.gif.');
    } else {
    // provide the form data
      var maxFileSize = 8;
      var fileSize = (fileToUpload.size);
      if (fileSize > maxFileSize * 1024 * 1024){
      alert(fileToUpload.name + " is " + Math.floor(fileToUpload.size/1024/1024)+ " Mbytes which exceeds the maximum allowed file upload size of " + maxFileSize + " Mbytes.");
    } else {
      progressBar.css({width:  "0%"});
      formData.append("userfile", fileToUpload);
      // now upload the file using $.ajax
      $.ajax({
          url: '/ajax/uploadProfile',
          type: 'post',
          data: formData,
          processData: false,
          contentType: false,
          success: function (data) {
            var obj = JSON.parse(data)
            var band_profile_image = "/files/profile/" + obj.band_profile_image;
            $('img#profilePic').attr("src", band_profile_image);
          },
          xhr: function () {
              var xhr = new XMLHttpRequest();
              xhr.upload.addEventListener("progress", function (event) {
                  if (event.lengthComputable) {
                      var percentComplete = Math.round((event.loaded / event.total) * 100);
                      // console.log(percentComplete);
                      $('.progress').show();
                      progressBar.css({width: percentComplete + "%"});
                      $("#upload-btn").attr("disabled", true);
                      progressBar.text(percentComplete + '%');
                  }
                  ;
              }, false);
              return xhr;
          }
      });
  }
}
}
});

//personal infoMessage

$.validator.addMethod("check_price_to", function (value, element) {
  var priceFrom = $('#price_from').val();
  var priceTo = $('#price_to').val();
   if (priceFrom > priceTo) {
     $.validator.messages.check_price_to = "Ovaj iznos mora biti veći";
     return false;
  }
  return true
});

$.validator.addMethod("check_price_from", function (value, element) {
  var priceFrom = $('#price_from').val();
  var priceTo = $('#price_to').val();
   if (priceFrom < priceTo) {
     $.validator.messages.check_price_from = "Ovaj iznos mora biti manji";
     return false;
  }
  return true
});

$.validator.addMethod("check_phone_format", function (value, element) {
  var phone = $('#phone').val();
  let regex = /(((\+|00))){1}[0-9]{10,12}\b/,
  result = regex.test(phone);

   if (result == false) {
     $.validator.messages.check_phone_format = "Telefon mora biti unet u formatu: 00381***** ili +381****";
     return false;
  }
  return true
});

function formPersonalInfo(){
  $("#formPersonalInfo").validate({
    ignore: ".ignore",
    rules: {
      email: {
        required: true,
        email: true
      },
      phone: {
        required: true,
        check_phone_format: true
      },
      city: "required",
      price_from: {required:true},
      price_to: {required:true, check_price_to:true },
      },
      messages: {
        city: {
          required: "Molimo Vas da popunite ovo polje",
        },
        phone: {
          required: "Molimo Vas da upišete broj telefona",
        },
        email: {
          required: "Molimo Vas da upišete email adresu",
          email: "Uneta adresa je u neispravnom formatu"
        },
        price_from: {
          required: "Molimo Vas da popunite ovo polje"
        },
        price_to: {
          required: "Molimo Vas da popunite ovo polje",
        },
    },
    errorElement: 'div',
    errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
        $(placement).append(error)
      } else {
        error.insertAfter(element);
      }
    },
    submitHandler: function(form) {
      var data = $('#formPersonalInfo').serialize();
      $.ajax({
        url: '/ajax/create_personal_information',
        type: 'POST',
        data: data,
        success: function(data) {
          $("#modal .message").html("Osnovne informacije su uspešno izmenjene");
          $("#modal .modal-icon").html('<i class="large material-icons green-text">check_circle_outline</i>');
          $('#modal').modal('open');
        }
      });
      return false;
    }
  });
};

//ajax sumbit parties
$("#submitParties").click(function (e) {
  e.preventDefault();
  var data = $('#formParties').serializeArray();
  var partiesArray = [];
   $.each($('input[type="checkbox"][name="party"]:checked'), function() {
   partiesArray.push($(this).val());
   });
   console.log(partiesArray);

  $.ajax({
    url: '/ajax/create_parties',
    type: 'POST',
    data: {data: partiesArray},
    success: function(data) {
      console.log(data);
      $("#modal .message").html("Vrsta proslave je uspešno izmenjena");
      $("#modal .modal-icon").html('<i class="large material-icons green-text">check_circle_outline</i>');
      $('#modal').modal('open');
    }
  });
});

//ajax sumbit parties
$("#submitGenres").click(function (e) {
  e.preventDefault();
  var data = $('#formGenres').serializeArray();
  var genresArray = [];
   $.each($('input[type="checkbox"][name="genre"]:checked'), function() {
   genresArray.push($(this).val());
   });

  $.ajax({
    url: '/ajax/create_genres',
    type: 'POST',
    data: {data: genresArray},
    success: function(data) {
      $("#modal .message").html("Žanrovi muzike su uspešno promenjeni");
      $("#modal .modal-icon").html('<i class="large material-icons green-text">check_circle_outline</i>');
      $('#modal').modal('open');
    }
  });
});

//ajax submit band_type
$("#submitBandType").click(function (e) {
  e.preventDefault();
  var data = $("input[name='bands_type']:checked").val();

  $.ajax({
    url: '/ajax/create_band_type',
    type: 'POST',
    data: {data: data},
    success: function(data) {
      console.log(data);
      $("#modal .message").html("Vrsta grupe je uspešno promenjena");
      $("#modal .modal-icon").html('<i class="large material-icons green-text">check_circle_outline</i>');
      $('#modal').modal('open');
    }
  });
});

//ajax submit instruments

$("#submitInstruments").click(function (e) {
  e.preventDefault();
  var data = $('#formInstruments').serializeArray();
  var genresInstruments = [];
   $.each($('input[type="checkbox"][name="instrument"]:checked'), function() {
   genresInstruments.push($(this).val());
   });

  $.ajax({
    url: '/ajax/create_instruments',
    type: 'POST',
    data: {data: genresInstruments},
    success: function(data) {
      $("#modal .message").html("Instrumenti su uspešno dodati");
      $("#modal .modal-icon").html('<i class="large material-icons green-text">check_circle_outline</i>');
      $('#modal').modal('open');
    }
  });
});

//ajax submit description

$("#submitDescription").click(function (e) {
  e.preventDefault();
  var description = $('#description').val();
  if (description) {
  console.log(description)

  $.ajax({
    url: '/ajax/create_description',
    type: 'POST',
    data: {data: description},
    success: function(data) {
      $("#modal .message").html("Kratak opis je uspešno dodat");
      $("#modal .modal-icon").html('<i class="large material-icons green-text">check_circle_outline</i>');
      $('#modal').modal('open');
    }
  });
  }
});

// ajax upload uploadPhoto

$('input#uploadPhoto').change(function(){
  var inputFile = $(this);
  var progressBar = $('.determinate.photo');
  var fileToUpload = inputFile[0].files[0];
  var formData = new FormData();

  if (fileToUpload == null) {
    alert("Izaberite sliku");

  } else {
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if(!allowedExtensions.exec(fileToUpload.name)){
      alert('Dozvoljeni formati su .jpeg/.jpg/.png/.gif.');
    } else {
    // provide the form data
      var maxFileSize = 8;
      var fileSize = (fileToUpload.size);
      if (fileSize > maxFileSize * 1024 * 1024){
      alert(fileToUpload.name + " is " + Math.floor(fileToUpload.size/1024/1024)+ " Mbytes which exceeds the maximum allowed file upload size of " + maxFileSize + " Mbytes.");
    } else {
      progressBar.css({width:  "0%"});
      formData.append("userfile", fileToUpload);
      $.ajax({
          url: '/ajax/uploadphotos',
          type: 'post',
          data: formData,
          processData: false,
          contentType: false,
          success: function (data) {
            var obj = JSON.parse(data)
            console.log(obj);
            $('.image_list').append('<div id="photo'+ obj.photo_id +'" class="col s6 m3"><a data-photoid="'+ obj.photo_id +'" data-photoname="' + obj.photo + '" class ="btn-floating btn-small red tooltipped removePhoto" data-position="top" data-tooltip="Obriši sliku"><i class="material-icons">close</i></a><img src="/files/photos/' + obj.photo +'"></div>');

          },
          xhr: function () {
              var xhr = new XMLHttpRequest();
              xhr.upload.addEventListener("progress", function (event) {
                  if (event.lengthComputable) {
                      var percentComplete = Math.round((event.loaded / event.total) * 100);
                      // console.log(percentComplete);
                      $('.progress').show();
                      progressBar.css({width: percentComplete + "%"});
                      progressBar.text(percentComplete + '%');
                  };
              }, false);
              return xhr;
          }
      });
  }
}
}
});

$('#editProfile .input[type="checkbox"]').change(function(){

        alert('Checked');

});

//remove picture
$(".image_list").on( "click", ".removePhoto", function(e) {
  e.preventDefault();
  var photo_id = $(this).data('photoid');
  var photo_name = $(this).data('photoname');
  var parent_id = $(this).parent().attr('id')
  console.log(photo_id, photo_name, parent_id);
  if (photo_id) {
    $.ajax({
        url: '/ajax/deletePhoto',
        type: 'post',
        data: {photo_id: photo_id, photo_name: photo_name},
        cache: false,
        success: function (data) {
          var obj = JSON.parse(data)
          //console.log(obj);
          $("#modal .message").html(obj);
          $("#modal .modal-icon").html('<i class="large material-icons green-text">check_circle_outline</i>');
          $('#modal').modal('open');
          $('#'+parent_id).css('display','none');
        }
      });
  };
});

$.validator.addMethod("check_youtube_format", function (value, element) {
  var link = value;
  if (link == )
  console.log (link);
  let regex = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/,
  result = regex.test(link);

   if (result == false) {
     $.validator.messages.check_youtube_format = "Neispravan Youtube link";
     return false;
  }
  return true
});

function formYoutube(){
  $("#formYoutube").validate({
    ignore: ".ignore",
    rules: {
      youtube1: {check_youtube_format:true },
      youtube2: {check_youtube_format:true },
      youtube3: {check_youtube_format:true },
      youtube4: {check_youtube_format:true },
      youtube5: {check_youtube_format:true },
      youtube6: {check_youtube_format:true },
      youtube7: {check_youtube_format:true },
      youtube8: {check_youtube_format:true },
      youtube9: {check_youtube_format:true },
      youtube10: {check_youtube_format:true },
      youtube11: {check_youtube_format:true },
      youtube12: {check_youtube_format:true },
      youtube13: {check_youtube_format:true },
      youtube14: {check_youtube_format:true },
      youtube15: {check_youtube_format:true },
      youtube16: {check_youtube_format:true },
      youtube17: {check_youtube_format:true },
      youtube18: {check_youtube_format:true },
      youtube19: {check_youtube_format:true },
      youtube20: {check_youtube_format:true }
      },

    errorElement: 'div',
    errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
        $(placement).append(error)
      } else {
        error.insertAfter(element);
      }
    },
    submitHandler: function(form) {
      var data = $('#formYoutube').serialize();
      console.log(data);
      $.ajax({
        url: '/ajax/create_youtube',
        type: 'POST',
        data: data,
        success: function(data) {
          $("#modal .message").html("Osnovne informacije su uspešno izmenjene");
          $("#modal .modal-icon").html('<i class="large material-icons green-text">check_circle_outline</i>');
          $('#modal').modal('open');
        }
      });
      return false;
    }
  });
};

//call functions
formPersonalInfo();
formYoutube();
$('body').addClass('loaded');




}); //end of document ready
