jQuery.extend( jQuery.fn.pickadate.defaults, {
    monthsFull: [ 'Januar', 'Februar', 'Mart', 'April', 'Maj', 'Jun', 'Jul', 'Avgust', 'Septembar', 'Oktobar', 'Novembar', 'Decembar' ],
    monthsShort: [ 'Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Avg', 'Sep', 'Okt', 'Nov', 'Dec' ],
    weekdaysFull: [ 'Nedelja', 'Ponedeljak', 'Utorak', 'Sreda', 'Četvrtak', 'Petak', 'Subota' ],
    weekdaysShort: [ 'Ned', 'Pon', 'Uto', 'Sre', 'Čet', 'Pet', 'Sub' ],
    today: 'danas',
    clear: 'izbriši',
	close: 'izlaz',
    firstDay: 1,
    //format: 'd. mmmm yyyy.',
	format: 'd. mmm yyyy.',
    formatSubmit: 'yyyy-mm-dd'
});